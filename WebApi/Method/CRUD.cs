﻿using MySqlConnector;
using System.Data;
using WebApi.Models;

namespace WebApi.Method
{
    public static class CRUD
    {        
        public static bool AddProduct(string conString, Product product)
        {
            try
            {
                // prepare connection
                using (var conn = new MySqlConnection(conString))
                {
                    conn.Open();

                    // prepare query command
                    using (var cmd = new MySqlCommand())
                    {                      
                        cmd.Connection = conn;
                        cmd.Transaction = conn.BeginTransaction();
                        cmd.CommandText = "INSERT INTO Product(title, `desc`, price, fk_category_id, keywords, image_content, image_name) " +
                                "VALUES (@title, @desc, @price, @fk_category_id, @keywords, @image_content, @image_name);";

                        var param = new MySqlParameter[]
                        {
                            new MySqlParameter { ParameterName="@title", DbType = DbType.String, Value = String.IsNullOrEmpty(product.title) ? "" : product.title },
                            new MySqlParameter { ParameterName="@desc", DbType = DbType.String, Value = String.IsNullOrEmpty(product.desc) ? "" : product.desc },
                            new MySqlParameter { ParameterName="@price", DbType = DbType.Int32, Value = product.price },
                            new MySqlParameter { ParameterName="@fk_category_id", DbType = DbType.Int32, Value = product.fk_category_id},
                            new MySqlParameter { ParameterName="@keywords", DbType = DbType.String, Value = String.IsNullOrEmpty(product.keywords) ? "" : product.keywords },
                            new MySqlParameter { ParameterName="@image_content", DbType = DbType.String, Value = String.IsNullOrEmpty(product.image_content) ? "" : product.image_content },
                            new MySqlParameter { ParameterName="@image_name", DbType = DbType.String, Value = String.IsNullOrEmpty(product.image_name) ? "" : product.image_name }
                        };

                        cmd.Parameters.AddRange(param);

                        try
                        {
                            int res = cmd.ExecuteNonQuery();
                            if (res == -1)
                            {
                                cmd.Transaction.Rollback();
                            }
                            else
                            {
                                cmd.Transaction.Commit();
                            }
                        }
                        catch
                        {
                            cmd.Transaction.Rollback();
                            throw;
                        }
                    }

                    conn.Close();
                }

                return true;
            }
            catch
            {
                throw;
            }
        }

        public static bool AddProductBlob(string conString, Product product)
        {
            try
            {
                string imageName = product.image_content_blob.FileName;

                // convert IFormFile ke byte[] untuk disimpan di BLOB (binary large object)
                byte[] imageContentBlob;

                using (MemoryStream stream = new MemoryStream())
                {
                    product.image_content_blob.CopyTo(stream);
                    imageContentBlob = stream.ToArray();
                }

                // prepare connection
                using (var conn = new MySqlConnection(conString))
                {
                    conn.Open();

                    // prepare query command
                    using (var cmd = new MySqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.Transaction = conn.BeginTransaction();
                        cmd.CommandText = "INSERT INTO Product(title, `desc`, price, fk_category_id, keywords, image_content, image_name, image_content_blob) " +
                                "VALUES (@title, @desc, @price, @fk_category_id, @keywords, @image_content, @image_name, @image_content_blob);";

                        var param = new MySqlParameter[]
                        {
                            new MySqlParameter { ParameterName="@title", DbType = DbType.String, Value = String.IsNullOrEmpty(product.title) ? "" : product.title },
                            new MySqlParameter { ParameterName="@desc", DbType = DbType.String, Value = String.IsNullOrEmpty(product.desc) ? "" : product.desc },
                            new MySqlParameter { ParameterName="@price", DbType = DbType.Int32, Value = product.price },
                            new MySqlParameter { ParameterName="@fk_category_id", DbType = DbType.Int32, Value = product.fk_category_id},
                            new MySqlParameter { ParameterName="@keywords", DbType = DbType.String, Value = String.IsNullOrEmpty(product.keywords) ? "" : product.keywords },
                            new MySqlParameter { ParameterName="@image_content", DbType = DbType.String, Value = String.IsNullOrEmpty(product.image_content) ? "" : product.image_content },
                            new MySqlParameter { ParameterName="@image_name", DbType = DbType.String, Value = String.IsNullOrEmpty(imageName) ? "" : imageName },
                            new MySqlParameter { ParameterName="@image_content_blob", DbType = DbType.Binary, Value = imageContentBlob },
                        };

                        cmd.Parameters.AddRange(param);

                        try
                        {
                            int res = cmd.ExecuteNonQuery();
                            if (res == -1)
                            {
                                cmd.Transaction.Rollback();
                            }
                            else
                            {
                                cmd.Transaction.Commit();
                            }
                        }
                        catch
                        {
                            cmd.Transaction.Rollback();
                            throw;
                        }
                    }

                    conn.Close();
                }

                return true;
            }
            catch
            {
                throw;
            }
        }
    }
}
