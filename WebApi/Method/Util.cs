﻿using System.Net.Mail;
using System.Text.RegularExpressions;

namespace WebApi.Method
{
    public class Util
    {
        public static object NullSafe(object data, object defaultValue)
        {
            return data == DBNull.Value ? defaultValue : data;
        }

        public static bool IsValidEmail(string emailaddress)
        {
            if (String.IsNullOrEmpty(emailaddress))
            {
                return false;
            }

            try
            {
                MailAddress m = new MailAddress(emailaddress);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        public static bool isValidAlphanumeric(string text)
        {
            if (String.IsNullOrEmpty(text))
            {
                return false;
            }

            Regex r = new Regex("^[a-zA-Z0-9]*$");
            if (r.IsMatch(text))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
