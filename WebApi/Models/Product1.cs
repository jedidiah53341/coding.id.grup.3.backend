﻿namespace WebApi.Models
{
    public class Product1
    {
        public int id { get; set; }
        public string? name { get; set; }
        public string? description { get; set; }
        public int brand_id { get; set; }
        public double price { get; set; }
        public int sold { get; set; }
        public int rating { get; set; }
        public string? brand_name { get; set; }
        public string? image { get; set; }

    }
}
