﻿namespace WebApi.Models
{
    public class OrderInvoiceItem
    {
        public int id { get; set; }
        public int order_invoice_id { get; set; }
        public int product_id { get; set; }
        public int quantity { get; set; }
        public double? total_price { get; set; }
        public string? status { get; set; }
        public string? created_at { get; set; }
        public string? updated_at { get; set; }
        public string? image { get; set; }
        public string? name { get; set; }
        public string? brand_name { get; set; }
        public int? order_invoice_item_id { get; set; }
    }
}
