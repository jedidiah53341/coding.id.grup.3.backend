﻿namespace WebApi.Models
{
    public class Brand
    {
        public int id { get; set; }
        public string? brand_name { get; set; }
        public string? description { get; set; }
        public string? image { get; set; }
    }
}
