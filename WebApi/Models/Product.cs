﻿namespace WebApi.Models
{
    public class Product
    {
        public int id { get; set; }
        public string title { get; set; }
        public string desc { get; set; }
        public int price { get; set; }
        public int fk_category_id { get; set; }
        public string keywords { get; set; }
        public string image_content { get; set; }
        public IFormFile image_content_blob { get; set; }
        public string image_name { get; set; }
    }
}
