﻿namespace WebApi.Models
{
    public class OrderInvoice
    {
        public int id { get; set; }
        public int customer_id { get; set; }
        public string? customer_email { get; set; }
        public string? customer_phone { get; set; }
        public string? payment_method { get; set; }
        public string? payment_verification_image { get; set; }
        public int product_total_quantity { get; set; }
        public double? payment_total { get; set; }
        public string? invoice_number { get; set; }
        public string? customer_address { get; set; }
        public string? status { get; set; }
        public string? created_at { get; set; }
        public string? updated_at { get; set; }
    }
}
