﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MySqlConnector;
using System.Data;
using WebApi.Models;
using WebApi.Method;

// AUTHHH
using Microsoft.AspNetCore.Authorization;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderInvoiceController : ControllerBase
    {
        private IConfiguration _configuration;
        private string _conString = "";

        public OrderInvoiceController(IConfiguration configuration)
        {
            _configuration = configuration;
            _conString = configuration["ConnectionStrings:Default"];
        }

        // list-all-order-invoices-belonging-to-a-user
        [HttpPost]
        [Route("list-all-order-invoices")]
        public ActionResult ListAllInvoices(string? customer_email)
        {
            try
            {
                var result = new List<OrderInvoice>();
                using (var conn = new MySqlConnection(_conString))
                {
                    conn.Open();
                    using (var cmd = new MySqlCommand())
                    {
                        string customer_emailValue = "";
                        if (customer_email is null)
                        {
                            cmd.CommandText = "SELECT * FROM order_invoices;";
                        } else
                        {
                            customer_emailValue = customer_email.ToString();
                            cmd.CommandText = "SELECT * FROM order_invoices WHERE customer_email LIKE @customer_email;";
                        }
                        cmd.Connection = conn;
                        cmd.Parameters.Add(new MySqlParameter { ParameterName = "@customer_email", DbType = DbType.String, Value = "%" + customer_emailValue + "%"});
                        MySqlDataReader reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                var tempProduct = new OrderInvoice();
                                tempProduct.id = reader["id"] == DBNull.Value ? 0 : (int)reader["id"];
                                tempProduct.customer_id = reader["customer_id"] == DBNull.Value ? 0 : (int)reader["customer_id"];
                                tempProduct.customer_email = reader["customer_email"] == DBNull.Value ? "" : (string)reader["customer_email"];
                                tempProduct.customer_phone = reader["customer_phone"] == DBNull.Value ? "" : (string)reader["customer_phone"];
                                tempProduct.payment_method = reader["payment_method"] == DBNull.Value ? "" : (string)reader["payment_method"];
                                tempProduct.payment_verification_image = reader["payment_verification_image"] == DBNull.Value ? "" : (string)reader["payment_verification_image"];
                                tempProduct.product_total_quantity = reader["product_total_quantity"] == DBNull.Value ? 0 : (int)reader["product_total_quantity"];
                                tempProduct.payment_total = reader["payment_total"] == DBNull.Value ? 0 : (double)reader["payment_total"];
                                tempProduct.customer_address = reader["customer_address"] == DBNull.Value ? "" : (string)reader["customer_address"];
                                tempProduct.invoice_number = reader["invoice_number"] == DBNull.Value ? "" : (string)reader["invoice_number"];
                                tempProduct.status = reader["status"] == DBNull.Value ? "" : (string)reader["status"];
                                tempProduct.created_at = reader["created_at"] == DBNull.Value ? "" : ((MySqlDateTime)reader["created_at"]).ToString();
                                tempProduct.updated_at = reader["updated_at"] == DBNull.Value ? "" : ((MySqlDateTime)reader["updated_at"]).ToString();
                                result.Add(tempProduct);

                            };
                        }
                        reader.Close();
                    }
                    conn.Close();
                }
                return StatusCode(200, result);
            }
            catch (Exception ex) { return BadRequest(ex.Message); }
        }

        // order-invoice
        [HttpPost]
        [Authorize(Roles = "buyer")]
        [Route("order-invoice")]
        public ActionResult OrderInvoice(int? invoiceId)
        {
            try
            {
                var result = new List<OrderInvoice>();
                using (var conn = new MySqlConnection(_conString))
                {
                    conn.Open();
                    using (var cmd = new MySqlCommand())
                    {
                        string invoiceIdValue = (invoiceId.Equals(null) ? "" : invoiceId.ToString());
                        cmd.CommandText = "SELECT * FROM order_invoices WHERE id = @invoiceId";
                        cmd.Connection = conn;
                        cmd.Parameters.Add(new MySqlParameter { ParameterName = "@invoiceId", DbType = DbType.String, Value = invoiceIdValue });
                        MySqlDataReader reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                var tempProduct = new OrderInvoice();
                                tempProduct.id = reader["id"] == DBNull.Value ? 0 : (int)reader["id"];
                                tempProduct.customer_id = reader["customer_id"] == DBNull.Value ? 0 : (int)reader["customer_id"];
                                tempProduct.customer_email = reader["customer_email"] == DBNull.Value ? "" : (string)reader["customer_email"];
                                tempProduct.customer_phone = reader["customer_phone"] == DBNull.Value ? "" : (string)reader["customer_phone"];
                                tempProduct.payment_method = reader["payment_method"] == DBNull.Value ? "" : (string)reader["payment_method"];
                                tempProduct.payment_verification_image = reader["payment_verification_image"] == DBNull.Value ? "" : (string)reader["payment_verification_image"];
                                tempProduct.product_total_quantity = reader["product_total_quantity"] == DBNull.Value ? 0 : (int)reader["product_total_quantity"];
                                tempProduct.payment_total = reader["payment_total"] == DBNull.Value ? 0 : (double)reader["payment_total"];
                                tempProduct.invoice_number = reader["invoice_number"] == DBNull.Value ? "" : (string)reader["invoice_number"];
                                tempProduct.customer_address = reader["customer_address"] == DBNull.Value ? "" : (string)reader["customer_address"];
                                tempProduct.status = reader["status"] == DBNull.Value ? "" : (string)reader["status"];
                                tempProduct.created_at = reader["created_at"] == DBNull.Value ? "" : ((MySqlDateTime)reader["created_at"]).ToString();
                                tempProduct.updated_at = reader["updated_at"] == DBNull.Value ? "" : ((MySqlDateTime)reader["updated_at"]).ToString();
                                result.Add(tempProduct);

                            };
                        }
                        reader.Close();
                    }
                    conn.Close();
                }
                return StatusCode(200, result);
            }
            catch (Exception ex) { return BadRequest(ex.Message); }
        }

        // order-invoice-by-user
        [HttpPost]
        [Authorize(Roles = "buyer")]
        [Route("order-invoice-by-user")]
        public ActionResult OrderInvoiceByUser(int? userId)
        {
            try
            {
                var result = new List<OrderInvoice>();
                using (var conn = new MySqlConnection(_conString))
                {
                    conn.Open();
                    using (var cmd = new MySqlCommand())
                    {
                        string userIdValue = (userId.Equals(null) ? "" : userId.ToString());
                        cmd.CommandText = "SELECT * FROM order_invoices WHERE customer_id = @userId AND NOT status = 'pending' ";
                        cmd.Connection = conn;
                        cmd.Parameters.Add(new MySqlParameter { ParameterName = "@userId", DbType = DbType.String, Value = userIdValue });
                        MySqlDataReader reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                var tempProduct = new OrderInvoice();
                                tempProduct.id = reader["id"] == DBNull.Value ? 0 : (int)reader["id"];
                                tempProduct.customer_id = reader["customer_id"] == DBNull.Value ? 0 : (int)reader["customer_id"];
                                tempProduct.customer_email = reader["customer_email"] == DBNull.Value ? "" : (string)reader["customer_email"];
                                tempProduct.customer_phone = reader["customer_phone"] == DBNull.Value ? "" : (string)reader["customer_phone"];
                                tempProduct.payment_method = reader["payment_method"] == DBNull.Value ? "" : (string)reader["payment_method"];
                                tempProduct.payment_verification_image = reader["payment_verification_image"] == DBNull.Value ? "" : (string)reader["payment_verification_image"];
                                tempProduct.product_total_quantity = reader["product_total_quantity"] == DBNull.Value ? 0 : (int)reader["product_total_quantity"];
                                tempProduct.payment_total = reader["payment_total"] == DBNull.Value ? 0 : (double)reader["payment_total"];
                                tempProduct.invoice_number = reader["invoice_number"] == DBNull.Value ? "" : (string)reader["invoice_number"];
                                tempProduct.customer_phone = reader["customer_address"] == DBNull.Value ? "" : (string)reader["customer_address"];
                                tempProduct.status = reader["status"] == DBNull.Value ? "" : (string)reader["status"];
                                tempProduct.created_at = reader["created_at"] == DBNull.Value ? "" : ((MySqlDateTime)reader["created_at"]).ToString();
                                tempProduct.updated_at = reader["updated_at"] == DBNull.Value ? "" : ((MySqlDateTime)reader["updated_at"]).ToString();
                                result.Add(tempProduct);

                            };
                        }
                        reader.Close();
                    }
                    conn.Close();
                }
                return StatusCode(200, result);
            }
            catch (Exception ex) { return BadRequest(ex.Message); }
        }

        // post-order-invoice
        [HttpPost]
        [Authorize(Roles = "admin")]
        [Route("post-order-invoice")]
        public ActionResult PostAnOrderInvoice([FromBody] OrderInvoiceBody body)
        {
            try
            {
                using (var conn = new MySqlConnection(_conString))
                {
                    conn.Open(); using (var cmd = new MySqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.Transaction = conn.BeginTransaction();
                        if (body.saveType == "add")
                        {
                            cmd.CommandText = "INSERT INTO order_invoices(customer_id,  customer_email,  customer_phone,  payment_method,  payment_total,  payment_verification_image, invoice_number, customer_address,  status,  created_at,  updated_at) " +
                                                                "VALUES (@customer_id, @customer_email, @customer_phone, @payment_method, @payment_total, @payment_verification_image, @invoice_number, @customer_address, @status, @created_at, @updated_at);";
                        }
                        cmd.Parameters.AddRange(new MySqlParameter[]
                        {
                        new MySqlParameter { ParameterName="@customer_id", DbType = DbType.Int32, Value = body.customer_id },
                        new MySqlParameter { ParameterName="@customer_email", DbType = DbType.String, Value = String.IsNullOrEmpty(body.customer_email) ? "" : body.customer_email },
                        new MySqlParameter { ParameterName="@customer_phone", DbType = DbType.String, Value = String.IsNullOrEmpty(body.customer_phone) ? "" : body.customer_phone },
                        new MySqlParameter { ParameterName="@payment_method", DbType = DbType.String, Value = String.IsNullOrEmpty(body.payment_method) ? "" : body.payment_method },
                        new MySqlParameter { ParameterName="@payment_verification_image", DbType = DbType.String, Value = String.IsNullOrEmpty(body.payment_verification_image) ? "" : body.payment_verification_image },
                        new MySqlParameter { ParameterName="@payment_total", DbType = DbType.Double, Value = body.payment_total},
                        new MySqlParameter { ParameterName="@invoice_number", DbType = DbType.String, Value = String.IsNullOrEmpty(body.invoice_number) ? "" : body.invoice_number },
                        new MySqlParameter { ParameterName="@customer_address", DbType = DbType.String, Value = String.IsNullOrEmpty(body.customer_address) ? "" : body.customer_address },
                        new MySqlParameter { ParameterName="@status", DbType = DbType.String, Value = String.IsNullOrEmpty(body.status) ? "" : body.status },
                        new MySqlParameter { ParameterName="@created_at",DbType =  DbType.String, Value = String.IsNullOrEmpty(body.created_at) ? "" : body.created_at },
                        new MySqlParameter { ParameterName="@updated_at", DbType = DbType.String, Value = String.IsNullOrEmpty(body.updated_at) ? "" : body.updated_at },
                        });

                        try { int res = cmd.ExecuteNonQuery(); if (res == -1) { cmd.Transaction.Rollback(); } { cmd.Transaction.Commit(); } } catch { cmd.Transaction.Rollback(); throw; }
                    }
                    conn.Close();
                }
                return Ok("Invoice pesanan baru berhasil ditambahkan");
            }
            catch (Exception ex) { return BadRequest(ex.Message); }
        }

        // edit-order-invoice
        [HttpPost]
        [Authorize]
        [Route("edit-order-invoice")]
        public ActionResult EditOrderInvoice([FromBody] OrderInvoiceBody body, int orderInvoiceId)
        {
            try
            {
                using (var conn = new MySqlConnection(_conString))
                {
                    conn.Open(); using (var cmd = new MySqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.Transaction = conn.BeginTransaction();
                        if (body.saveType == "edit")
                        {
                            cmd.CommandText = "UPDATE order_invoices SET customer_id = @customer_id,  customer_email = @customer_email,  customer_phone = @customer_phone, payment_method = @payment_method, payment_total = @payment_total,  payment_verification_image = @payment_verification_image, invoice_number = @invoice_number, customer_address = @customer_address, status = @status,  created_at = @created_at,  updated_at = @updated_at " +
                                                          "WHERE id = @orderInvoiceId;";
                        }
                        cmd.Parameters.AddRange(new MySqlParameter[]
                        {
                        new MySqlParameter { ParameterName="@customer_id", DbType = DbType.Int32, Value = body.customer_id },
                        new MySqlParameter { ParameterName="@customer_email", DbType = DbType.String, Value = String.IsNullOrEmpty(body.customer_email) ? "" : body.customer_email },
                        new MySqlParameter { ParameterName="@customer_phone", DbType = DbType.String, Value = String.IsNullOrEmpty(body.customer_phone) ? "" : body.customer_phone },
                        new MySqlParameter { ParameterName="@payment_method", DbType = DbType.String, Value = String.IsNullOrEmpty(body.payment_method) ? "" : body.payment_method },
                        new MySqlParameter { ParameterName="@payment_verification_image", DbType = DbType.String, Value = String.IsNullOrEmpty(body.payment_verification_image) ? "" : body.payment_verification_image },
                        new MySqlParameter { ParameterName="@payment_total", DbType = DbType.Double, Value = body.payment_total},
                        new MySqlParameter { ParameterName="@invoice_number", DbType = DbType.String, Value = String.IsNullOrEmpty(body.invoice_number) ? "" : body.invoice_number },
                        new MySqlParameter { ParameterName="@customer_address", DbType = DbType.String, Value = String.IsNullOrEmpty(body.customer_address) ? "" : body.customer_address },
                        new MySqlParameter { ParameterName="@status", DbType = DbType.String, Value = String.IsNullOrEmpty(body.status) ? "" : body.status },
                        new MySqlParameter { ParameterName="@created_at",DbType =  DbType.String, Value = String.IsNullOrEmpty(body.created_at) ? "" : body.created_at },
                        new MySqlParameter { ParameterName="@updated_at", DbType = DbType.String, Value = String.IsNullOrEmpty(body.updated_at) ? "" : body.updated_at },
                        new MySqlParameter { ParameterName="@orderInvoiceId", DbType = DbType.Int32, Value = orderInvoiceId},
                        });

                        try { int res = cmd.ExecuteNonQuery(); if (res == -1) { cmd.Transaction.Rollback(); } { cmd.Transaction.Commit(); } } catch { cmd.Transaction.Rollback(); throw; }
                    }
                    conn.Close();
                }
                return Ok("Invoice pesanan berhasil diedit");
            }
            catch (Exception ex) { return BadRequest(ex.Message); }
        }

        // delete-order-invoice
        [HttpPost]
        [Authorize(Roles = "admin")]
        [Route("delete-order-invoice")]
        public ActionResult DeleteOrderInvoice(int orderInvoiceId)
        {
            try
            {
                using (var conn = new MySqlConnection(_conString))
                {
                    conn.Open(); using (var cmd = new MySqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.Transaction = conn.BeginTransaction();
                        cmd.CommandText = "DELETE FROM order_invoices WHERE id = @orderInvoiceId;";
                        cmd.Parameters.AddRange(new MySqlParameter[]
                        {
                        new MySqlParameter { ParameterName="@orderInvoiceId", DbType = DbType.Int32, Value = orderInvoiceId},
                        });

                        try { int res = cmd.ExecuteNonQuery(); if (res == -1) { cmd.Transaction.Rollback(); } { cmd.Transaction.Commit(); } } catch { cmd.Transaction.Rollback(); throw; }
                    }
                    conn.Close();
                }
                return Ok("Invoice pesanan berhasil dihapus");
            }
            catch (Exception ex) { return BadRequest(ex.Message); }
        }

        // checkout
        [HttpPost]
        [Route("checkout")]
        public ActionResult Checkout([FromBody] OrderInvoiceBody body, int orderInvoiceId)
        {
            try
            {
                var result = new List<String>();
                var invoice_number = 0;
                bool hasSomeNotIncludedItems = false;
                int customer_id = 0;
                int temporaryOrderInvoiceId = 0;
                double payment_total = 0;
                int productTotalQuantity = 0;
                var productIdsToUpdateTheirSoldQuantity = new List<int>();
                var productQuantitiesWhichAreBeingCheckedOut = new List<int>();
                int currentSoldCount = 0;
                using (var conn = new MySqlConnection(_conString))
                {

                    // Ambil data order invoice yang ingin di checkout, ambil id-nya dan customer_id-nya.
                    conn.Open();
                    using (var cmd = new MySqlCommand())
                    {
                        string orderInvoiceIdValue = (orderInvoiceId.Equals(null) ? "" : orderInvoiceId.ToString());
                        cmd.CommandText = "SELECT * FROM order_invoices WHERE id = @orderInvoiceId;";
                        cmd.Connection = conn;
                        cmd.Parameters.Add(new MySqlParameter { ParameterName = "@orderInvoiceId", DbType = DbType.String, Value = orderInvoiceIdValue });
                        MySqlDataReader reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                invoice_number = (int)reader["id"];
                                customer_id = (int)reader["customer_id"];
                            }
                        }
                        reader.Close();
                    }
                    conn.Close();

                    // Cek apakah terdapat beberapa item di cart yang tidak diikutkan ( checkboxnya tidak dicentang ) ketika checkout, ubah nilai boolean hasSomeNotIncludedItems
                    conn.Open();
                    using (var cmd = new MySqlCommand())
                    {
                        string orderInvoiceIdValue = (orderInvoiceId.Equals(null) ? "" : orderInvoiceId.ToString());
                        cmd.CommandText = "SELECT * FROM order_invoice_items WHERE order_invoice_id = @orderInvoiceId AND status = 'notIncluded';";
                        cmd.Connection = conn;
                        cmd.Parameters.Add(new MySqlParameter { ParameterName = "@orderInvoiceId", DbType = DbType.String, Value = orderInvoiceIdValue });
                        MySqlDataReader reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                hasSomeNotIncludedItems = true;
                            }
                        }
                        reader.Close();
                    }
                    conn.Close();

                    // Kalau ada beberapa barang yang tidak disertakan dalam checkout, maka :
                    // Segera buatlah sebuah order invoice baru dengan status khusus yaitu 'temporary'. 
                    // Order invoice baru ini akan menampung barang" yang tak disertakan dalam checkout tersebut.
                    if ( hasSomeNotIncludedItems )
                    {
                        conn.Open(); using (var cmd = new MySqlCommand())
                        {
                            cmd.Connection = conn;
                            cmd.Transaction = conn.BeginTransaction();
                                cmd.CommandText = "INSERT INTO order_invoices(customer_id,  customer_email,  customer_phone,  payment_method,  payment_total,  payment_verification_image,  invoice_number, status,  created_at,  updated_at) " +
                                                                     "VALUES (@customer_id, '' , '', '', 0, '',@invoice_number, 'temporary' , '', '');";
                            cmd.Parameters.AddRange(new MySqlParameter[] {
                            new MySqlParameter { ParameterName="@customer_id", DbType = DbType.Int32, Value = customer_id },
                            new MySqlParameter { ParameterName="@invoice_number", DbType = DbType.String, Value = "2022" + (invoice_number+1).ToString()  },
                            });
                            try { int res = cmd.ExecuteNonQuery(); if (res == -1) { cmd.Transaction.Rollback(); } { cmd.Transaction.Commit(); } } catch { cmd.Transaction.Rollback(); throw; }
                        }
                        conn.Close();

                        // Setelah membuat cart temporary, ambil id-nya.
                        conn.Open();
                        using (var cmd = new MySqlCommand())
                        {
                            cmd.CommandText = "SELECT * FROM order_invoices WHERE status = 'temporary' ;";
                            cmd.Connection = conn;
                            MySqlDataReader reader = cmd.ExecuteReader();
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    temporaryOrderInvoiceId = (int)reader["id"];;
                                    System.Diagnostics.Debug.WriteLine("temp order invoice is " + temporaryOrderInvoiceId.ToString());
                                }
                            }
                            reader.Close();
                        }
                        conn.Close();

                        // Untuk setiap item yang tidak disertakan dalam checkout, ubah/pindahkan order_invoice_id mereka menjadi id order invoice temporary tersebut.
                        conn.Open(); using (var cmd = new MySqlCommand())
                        {
                            cmd.Connection = conn;
                            cmd.Transaction = conn.BeginTransaction();
                            cmd.CommandText = "UPDATE order_invoice_items SET order_invoice_id = @temporaryOrderInvoiceId " +
                                                              "WHERE order_invoice_id = @orderInvoiceId AND status = 'notIncluded' ;";
                            cmd.Parameters.AddRange(new MySqlParameter[] {
                            new MySqlParameter { ParameterName="@orderInvoiceId", DbType = DbType.Int32, Value = orderInvoiceId},
                            new MySqlParameter { ParameterName="@temporaryOrderInvoiceId", DbType = DbType.Int32, Value = temporaryOrderInvoiceId},
                            });

                            // *console.log()
                            System.Diagnostics.Debug.WriteLine("Order invoice id is " + orderInvoiceId.ToString());

                            try { int res = cmd.ExecuteNonQuery(); if (res == -1) { cmd.Transaction.Rollback(); } { cmd.Transaction.Commit(); } } catch { cmd.Transaction.Rollback(); throw; }
                        }
                        conn.Close();

                    }

                    // Ambil kembali data order invoice item sesuai invoice yang ingin dicheckout, kemudian hitung total jumlah produknya.
                    // Di titik ini, barang" yang tak disertakan ketika checkout tak akan terhitung karena mereka sudah memiliki order_invoice_id yang berbeda, yaitu yang invoice temporary.
                    conn.Open(); using (var cmd = new MySqlCommand())
                    {
                        string orderInvoiceIdValue = orderInvoiceId.ToString();
                        cmd.CommandText = "SELECT * FROM order_invoice_items WHERE order_invoice_id = @orderInvoiceId;";
                        cmd.Connection = conn;
                        cmd.Parameters.Add(new MySqlParameter { ParameterName = "@orderInvoiceId", DbType = DbType.String, Value = orderInvoiceId });
                        MySqlDataReader reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                productTotalQuantity += (int)reader["quantity"];
                            }
                        }

                        reader.Close();
                    }
                    conn.Close();

                    // Kita akan mengambil data jumlah item yang di checkout, dan digunakan data tersebut untuk update data kolom 'sold' di tabel 'products'.
                    //  Ambil daftar seluruh invoice items yang memiliki order_invoice_id sesuai invoice id yang ingin di-checkout, dan statusnya 'included'
                    conn.Open();
                    using (var cmd = new MySqlCommand())
                    {
                        string orderInvoiceIdValue = (orderInvoiceId.Equals(null) ? "" : orderInvoiceId.ToString());
                        cmd.CommandText = "SELECT * FROM order_invoice_items WHERE order_invoice_id = @orderInvoiceId AND status = 'included';";
                        cmd.Connection = conn;
                        cmd.Parameters.Add(new MySqlParameter { ParameterName = "@orderInvoiceId", DbType = DbType.String, Value = orderInvoiceIdValue });
                        MySqlDataReader reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                productIdsToUpdateTheirSoldQuantity.Add((int)reader["product_id"]);
                                productQuantitiesWhichAreBeingCheckedOut.Add((int)reader["quantity"]);

                            }
                        }
                        reader.Close();
                    }
                    conn.Close();

                    // Lakukan update
                    for (int i = 0; i < productIdsToUpdateTheirSoldQuantity.Count; i++)
                    {

                        conn.Open();
                        using (var cmd = new MySqlCommand())
                        {
                            cmd.CommandText = "SELECT * FROM products WHERE id = @productsId;";
                            cmd.Connection = conn;
                            cmd.Parameters.Add(new MySqlParameter { ParameterName = "@productsId", DbType = DbType.String, Value = productIdsToUpdateTheirSoldQuantity[i] });
                            MySqlDataReader reader = cmd.ExecuteReader();
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    currentSoldCount = (int)reader["sold"];

                                }
                            }
                            reader.Close();
                        }
                        conn.Close();

                        conn.Open(); using (var cmd = new MySqlCommand())
                        {
                            cmd.Connection = conn;
                            cmd.Transaction = conn.BeginTransaction();
                            cmd.CommandText = "UPDATE products SET sold = @sold " +
                                                              "WHERE id = @productsId;";
                            cmd.Parameters.AddRange(new MySqlParameter[] {
                            new MySqlParameter { ParameterName="@sold", DbType = DbType.Int32, Value = productQuantitiesWhichAreBeingCheckedOut[i] + currentSoldCount},
                            new MySqlParameter { ParameterName="@productsId", DbType = DbType.Int32, Value = productIdsToUpdateTheirSoldQuantity[i]},
                            });
                            try { int res = cmd.ExecuteNonQuery(); if (res == -1) { cmd.Transaction.Rollback(); } { cmd.Transaction.Commit(); } } catch { cmd.Transaction.Rollback(); throw; }
                        }
                        conn.Close();
                    }

                    // Update status order invoice, terutama data payment_total dan product_total_quantity
                    conn.Open(); using (var cmd = new MySqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.Transaction = conn.BeginTransaction();
                        if (body.saveType == "edit")
                        {
                            cmd.CommandText = "UPDATE order_invoices SET customer_id = @customer_id,  customer_email = @customer_email,  customer_phone = @customer_phone, payment_method = @payment_method, product_total_quantity = @productTotalQuantity,  payment_verification_image = @payment_verification_image, invoice_number = @invoice_number, customer_address = @customer_address, status = 'Checkout',  created_at = @created_at,  updated_at = @updated_at " +
                                                          "WHERE id = @orderInvoiceId;";
                        }
                        cmd.Parameters.AddRange(new MySqlParameter[]
                        {
                        new MySqlParameter { ParameterName="@customer_id", DbType = DbType.Int32, Value = body.customer_id },
                        new MySqlParameter { ParameterName="@customer_email", DbType = DbType.String, Value = String.IsNullOrEmpty(body.customer_email) ? "" : body.customer_email },
                        new MySqlParameter { ParameterName="@customer_phone", DbType = DbType.String, Value = String.IsNullOrEmpty(body.customer_phone) ? "" : body.customer_phone },
                        new MySqlParameter { ParameterName="@payment_method", DbType = DbType.String, Value = String.IsNullOrEmpty(body.payment_method) ? "" : body.payment_method },
                        new MySqlParameter { ParameterName="@payment_verification_image", DbType = DbType.String, Value = String.IsNullOrEmpty(body.payment_verification_image) ? "" : body.payment_verification_image },
                        new MySqlParameter { ParameterName="@payment_total", DbType = DbType.Double, Value = body.payment_total},
                        new MySqlParameter { ParameterName="@invoice_number", DbType = DbType.String, Value = "2022" + invoice_number.ToString() },
                        new MySqlParameter { ParameterName="@customer_address", DbType = DbType.String, Value = String.IsNullOrEmpty(body.customer_address) ? "" : body.customer_address },
                        new MySqlParameter { ParameterName="@status", DbType = DbType.String, Value = String.IsNullOrEmpty(body.status) ? "" : body.status },
                        new MySqlParameter { ParameterName="@created_at",DbType =  DbType.String, Value = String.IsNullOrEmpty(body.created_at) ? "" : body.created_at },
                        new MySqlParameter { ParameterName="@updated_at", DbType = DbType.String, Value = String.IsNullOrEmpty(body.updated_at) ? "" : body.updated_at },
                        new MySqlParameter { ParameterName="@orderInvoiceId", DbType = DbType.Int32, Value = orderInvoiceId},
                        new MySqlParameter { ParameterName="@productTotalQuantity", DbType = DbType.Double, Value = productTotalQuantity },
                        });
                        try { int res = cmd.ExecuteNonQuery(); if (res == -1) { cmd.Transaction.Rollback(); } { cmd.Transaction.Commit(); } } catch { cmd.Transaction.Rollback(); throw; }
                    }
                    conn.Close();


                    // Setelahnya, kita perlu mengevaluasi juga total quantity dan total price dari invoice baru / invoice temporary.
                    // Invoice temporary tersebut kini kita ubah statusnya menjadi pending, sebab invoice yang sebelumnya ( yang originalnya ) sudah
                    // di update statusnya menjadi 'Checkout' -- dengan kata lain, sudah di checkout.
                    productTotalQuantity = 0;
                    if (hasSomeNotIncludedItems)
                    {
                        conn.Open(); using (var cmd = new MySqlCommand())
                        {
                            cmd.Connection = conn;
                            cmd.Transaction = conn.BeginTransaction();
                            cmd.CommandText = "UPDATE order_invoices SET status = 'pending' " +
                                                              "WHERE id = @temporaryOrderInvoiceId;";
                            cmd.Parameters.AddRange(new MySqlParameter[] {
                            new MySqlParameter { ParameterName="@temporaryOrderInvoiceId", DbType = DbType.Int32, Value = temporaryOrderInvoiceId},
                            });
                            result.Add("Temporary order_invoice has been made pending." );
                            result.Add(temporaryOrderInvoiceId.ToString());
                            try { int res = cmd.ExecuteNonQuery(); if (res == -1) { cmd.Transaction.Rollback(); } { cmd.Transaction.Commit(); } } catch { cmd.Transaction.Rollback(); throw; }
                        }
                        conn.Close();

                        // Sama seperti proses untuk hitung total quantity dan total payment terhadap invoice yang ingin dicheckout
                        // bedanya, kini hitung untuk invoice yang bersifat temporary.
                        conn.Open(); using (var cmd = new MySqlCommand())
                        {
                            string orderInvoiceIdValue = orderInvoiceId.ToString();
                            cmd.CommandText = "SELECT * FROM order_invoice_items WHERE order_invoice_id = @orderInvoiceId;";
                            cmd.Connection = conn;
                            cmd.Parameters.Add(new MySqlParameter { ParameterName = "@orderInvoiceId", DbType = DbType.String, Value = temporaryOrderInvoiceId });
                            MySqlDataReader reader = cmd.ExecuteReader();
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    productTotalQuantity += (int)reader["quantity"];
                                    payment_total += (double)reader["total_price"];
                                }
                            }
                           
                            reader.Close();
                        }
                        conn.Close();

                        // Sama seperti proses untuk hitung total quantity dan total payment terhadap invoice yang ingin dicheckout
                        // bedanya, kini hitung untuk invoice yang bersifat temporary.
                        // Terapkan updatenya
                        conn.Open(); using (var cmd = new MySqlCommand())
                        {
                            cmd.Connection = conn;
                            cmd.Transaction = conn.BeginTransaction();
                            cmd.CommandText = "UPDATE order_invoices SET payment_total = @paymentTotal, product_total_quantity = @productTotalQuantity WHERE id = @orderInvoiceId;";
                            cmd.Parameters.AddRange(new MySqlParameter[]
                            {
                            new MySqlParameter { ParameterName="@orderInvoiceId", DbType = DbType.Int32, Value = temporaryOrderInvoiceId },
                            new MySqlParameter { ParameterName="@paymentTotal", DbType = DbType.Double, Value = payment_total },
                            new MySqlParameter { ParameterName="@productTotalQuantity", DbType = DbType.Double, Value = productTotalQuantity },
                            });
                            try { int res = cmd.ExecuteNonQuery(); if (res == -1) { cmd.Transaction.Rollback(); } { cmd.Transaction.Commit(); } } catch { cmd.Transaction.Rollback(); throw; }
                        }
                        System.Diagnostics.Debug.WriteLine("PTQ is " + productTotalQuantity.ToString());
                        conn.Close();

                    } else
                    {
                        result.Add("No temporary order_invoice made.");
                    }

                }
                return StatusCode(200, result);
            }
            catch (Exception ex) { return BadRequest(ex.Message); }
        }

    }
}
