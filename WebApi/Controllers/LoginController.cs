﻿﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MySqlConnector;

// CRYPTO AND AUTHHH
using System.Security.Cryptography;
using System.Text;
using System.Data;

// AUTHHH
using System.Security.Claims;

// DOWNLOAD AUTHHH
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;

using WebApi.Method;
using WebApi.Models;
using Microsoft.AspNetCore.Authorization;


namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        public string conString = "";
        public string jwtKey = "";
        public string jwtKeyEmail = "KEY UNTUK JWT SEND EMAIL";
        public string frontUrl = "";

        // CONSTRUCTOR
        public LoginController(IConfiguration configuration)
        {
            frontUrl = configuration["Url:Frontend"];
            conString = configuration["ConnectionStrings:Default"];
            jwtKey = configuration["jwt:key"];
        }

        [HttpPost]
        [Route("Register")]
        public ActionResult Register([FromBody] UserBody body)
        {


            using (var conn = new MySqlConnection(conString))
            {
                conn.Open();

                using (var cmd = new MySqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = "INSERT INTO users(username, customer_name, email, passwordHash, passwordSalt, role, active) VALUES (@username, @customer_name, @email, @hash, @salt, @role, @active);";

                    // HASH AND SALT GENERATED HERE
                    byte[] salt;
                    byte[] hash;
                    using (var hmac = new HMACSHA512())
                    {
                        salt = hmac.Key;
                        hash = hmac.ComputeHash(Encoding.UTF8.GetBytes(body.password));
                    }

                    cmd.Parameters.AddRange(new MySqlParameter[]
                    {
                        new MySqlParameter {ParameterName="@username", DbType=DbType.String, Value = body.username},
                        new MySqlParameter {ParameterName="@customer_name", DbType=DbType.String, Value = body.customer_name},
                        new MySqlParameter {ParameterName="@email", DbType=DbType.String, Value = body.email},
                        new MySqlParameter {ParameterName="@hash", DbType=DbType.Binary, Value = hash},
                        new MySqlParameter {ParameterName="@salt", DbType=DbType.Binary, Value = salt},
                        new MySqlParameter {ParameterName="@role", DbType=DbType.String, Value = body.role},
                        new MySqlParameter {ParameterName="@active", MySqlDbType=MySqlDbType.Bool, Value = false},
                    });

                    // execute query and validate it
                    try
                    {
                        int executeOk = cmd.ExecuteNonQuery();
                    }
                    catch
                    {
                        cmd.Transaction.Rollback();
                        throw new Exception("REGISTER DATA FAILED");
                    }

                    // CREATE TOKEN FOR EMAIL
                    List<Claim> claims = new List<Claim>
                        {
                            new Claim("@email", body.email),
                            new Claim("@request_activation", "true"),
                        };
                    string verifToken = Token.CreateJwtToken(claims, jwtKeyEmail);
                    //throw new Exception(verifToken);

                    // SEND EMAIL
                    string targetUrl = frontUrl + "/validation/" + verifToken;
                    string emailBody = @$"
                        <div style='border: 1px solid #dadce0; border-radius: 10px; padding: 30px; margin: 0 20%;'>
                            <p style='font-size: 20px; color:#FF7596; text-align:center; margin-bottom: -20px;'> 
                                1Electronic 
                            </p>
                            <p style='text-align:center; font-size:30px; font-weight: bold;'>
                                Verifikasi Pengguna Baru
                            </p>

                            <div style='display: block;'>
                                <p style='text-align: center;'>
                                    Untuk menyelesaikan pengaturan akun Anda, kami hanya perlu memastikan alamat email ini milik Anda.
                                    Untuk memverifikasi alamat email Anda, klik tombol Verifikasi / link dibawah ini.
                                </P>
                                <div style='text-align: center;'>
                                    <button>VERIFIKASI</button>
                                </div>
                                <p style='text-align: center;'>
                                    <a href={targetUrl}>{targetUrl}</a>
                                </p>
                            </div>
                        </div> ";

                    try
                    {
                        Email.SendEmail(body.email, "VERIFY YOUR EMAIL", emailBody);
                    }
                    catch
                    {
                        cmd.Transaction.Rollback();
                        throw new Exception("SEND EMAIL FAILED");
                    }

                }


                conn.Close();
            }

            return Ok();
        }



        [HttpPost]
        [Route("Login")]
        public ActionResult Login([FromBody] UserBody body)
        {
            bool isTrueUser = false;
            string jwtToken = "";
            string userId = "";
            string userRole = "";
            int orderInvoiceId = 0;

            using (var conn = new MySqlConnection(conString))
            {

                conn.Open();

                using (var cmd = new MySqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = "SELECT * FROM users WHERE username = @username;";
                    cmd.Parameters.Add(new MySqlParameter { ParameterName = "@username", DbType = DbType.String, Value = body.username });

                    MySqlDataReader reader = cmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {

                            // VERIFIKASI USER AKTIF
                            bool active = (bool)Util.NullSafe(reader["active"], false);
                            if (active == false)
                            {
                                //throw new Exception("USER IS INACTIVE, PLEASE CONTANT US TO RE-ACTIVE IT");
                                return BadRequest("Anda Belum Verifikasi Akun, Silahkan Cek Email Untuk Verifikasi");
                            }


                            // CHECK HASH DB DENGAN PASSWORD YANG DIINPUT        
                            byte[] userHash = (byte[])reader["passwordHash"];
                            byte[] userSalt = (byte[])reader["passwordSalt"];

                            using (var hmac = new HMACSHA512(userSalt))
                            {
                                byte[] checkHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(body.password));
                                isTrueUser = checkHash.SequenceEqual(userHash);
                            }

                            // CREATE TOKEN
                            if (isTrueUser)
                            {
                                string role = reader["role"].ToString();
                                userId = reader["id"].ToString();
                                userRole = reader["role"].ToString();
                                jwtToken = PISAH.CreateToken(new UserBody
                                {
                                    username = body.username,
                                    role = role,
                                }, jwtKey);
                            }

                        }


                    }
                    else
                    {
                        return BadRequest("Username salah");
                    }


                }

                conn.Close();

                conn.Open();
                using (var cmd = new MySqlCommand())
                {
                    string loggedInUserIdValue = (userId.Equals(null) ? "" : userId.ToString());
                    cmd.CommandText = "SELECT * FROM order_invoices WHERE customer_id = @loggedInUserId AND status = 'pending';";
                    cmd.Connection = conn;
                    cmd.Parameters.Add(new MySqlParameter { ParameterName = "@loggedInUserId", DbType = DbType.String, Value = loggedInUserIdValue });
                    MySqlDataReader reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            orderInvoiceId = (int)reader["id"];
                        }
                    }
                    reader.Close();
                }
                conn.Close();


            }

            if (isTrueUser)
            {
                var result = new List<String>();
                result.Add(userId); result.Add(userRole);
                if (orderInvoiceId != 0)
                {
                    result.Add("Pending invoice exists!");
                    result.Add(orderInvoiceId.ToString());
                }
                result.Add(jwtToken);
                return Ok(result);
            }
            else
            {
                return BadRequest("PASSWORD SALAH");
            }
        }

        [HttpPost]
        [Route("VerifyEmailToken")]
        public ActionResult VerifyEmailToken([FromBody] TokenBody body)
        {
            try
            {

                string verifToken = body.verifToken;

                // VERIFY CLAIMS
                List<Claim> claims = Token.ParseJwtTokenClaim(verifToken, jwtKeyEmail);

                bool isActivation = false;
                string email = "";

                foreach (Claim claim in claims)
                {
                    if (claim.Type == "@request_activation")
                    {
                        isActivation = true;
                    }
                    if (claim.Type == "@email")
                    {
                        email = claim.Value;
                    }
                }

                // CLAIM ALREADY VERIFIED
                if (isActivation == true && String.IsNullOrEmpty(email) == false)
                {
                    using (var conn = new MySqlConnection(conString))
                    {
                        conn.Open();

                        using (var cmd = new MySqlCommand())
                        {
                            // ACTIVATE USER
                            cmd.Connection = conn;
                            cmd.CommandText = "UPDATE users SET `active` = true WHERE email=@email";
                            cmd.Parameters.Add(new MySqlParameter { ParameterName = "@email", MySqlDbType = MySqlDbType.VarString, Value = email });
                            cmd.Transaction = conn.BeginTransaction();

                            int executeOk = cmd.ExecuteNonQuery();
                            if (executeOk == -1)
                            {
                                cmd.Transaction.Rollback();
                                throw new Exception("USER ACTIVATION FAILED");
                            }

                            cmd.Transaction.Commit();
                        }

                        conn.Close();
                    }
                }
                else
                {
                    throw new Exception("EMAIL TOKEN VERIFICATION FAILED");
                }

                return Ok("email verified");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }






    }
}

public static class PISAH
{
    public static string CreateToken(UserBody user, string jwtKey)
    {
        List<Claim> claim = new List<Claim>
        {
            new Claim(ClaimTypes.Name, user.username),
            new Claim(ClaimTypes.Role, user.role)
        };

        var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtKey));
        var cred = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

        var token = new JwtSecurityToken(
            claims: claim,
            expires: DateTime.Now.AddDays(1),
            signingCredentials: cred);

        var jwtToken = new JwtSecurityTokenHandler().WriteToken(token);

        return jwtToken;
    }
}

public class UserDb
{
    public int? id { get; set; }
    public string? username { get; set; }
    public string? customer_name { get; set; }
    public string? email { get; set; }
    public byte[]? passwordHash { get; set; }
    public byte[]? passwordSalt { get; set; }
    public string? role { get; set; }
}

public class UserBody
{
    public string? username { get; set; }
    public string? customer_name { get; set; }
    public string? email { get; set; }
    public string? password { get; set; }
    public string? role { get; set; }
}

public class TokenBody
{
    public string verifToken { get; set; }
}