﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MySqlConnector;
using System.Data;
using WebApi.Models;
using WebApi.Method;

// AUTHHH
using Microsoft.AspNetCore.Authorization;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderInvoiceItemController : ControllerBase
    {
        private IConfiguration _configuration;
        private string _conString = "";

        public OrderInvoiceItemController(IConfiguration configuration)
        {
            _configuration = configuration;
            _conString = configuration["ConnectionStrings:Default"];
        }

        // list-all-order-invoices-belonging-to-an-order-invoice
        [HttpPost]
        [Route("list-all-order-invoice-items")]
        public ActionResult ListAllInvoices(int? orderInvoiceId)
        {
            try
            {
                var result = new List<OrderInvoiceItem>();
                using (var conn = new MySqlConnection(_conString))
                {
                    conn.Open();
                    using (var cmd = new MySqlCommand())
                    {
                        string orderInvoiceIdValue = (orderInvoiceId.Equals(null) ? "" : orderInvoiceId.ToString());
                        cmd.CommandText = "SELECT " +
                            "order_invoice_items.id, order_invoice_items.order_invoice_id, order_invoice_items.product_id, order_invoice_items.quantity, " +
                            "order_invoice_items.total_price, order_invoice_items.status, order_invoice_items.created_at,order_invoice_items.updated_at, " +
                            "products.name, brands.brand_name " +
                            "FROM order_invoice_items " +
                            "JOIN products ON products.id = order_invoice_items.product_id " +
                            "JOIN brands ON products.brand_id = brands.id " +
                            "WHERE order_invoice_items.order_invoice_id = @orderInvoiceId ;";
                        cmd.Connection = conn;
                        cmd.Parameters.Add(new MySqlParameter { ParameterName = "@orderInvoiceId", DbType = DbType.String, Value = orderInvoiceIdValue });
                        MySqlDataReader reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                var tempProduct = new OrderInvoiceItem();
                                tempProduct.id = reader["id"] == DBNull.Value ? 0 : (int)reader["id"];
                                tempProduct.order_invoice_id = reader["order_invoice_id"] == DBNull.Value ? 0 : (int)reader["order_invoice_id"];
                                tempProduct.product_id = reader["product_id"] == DBNull.Value ? 0 : (int)reader["product_id"];
                                tempProduct.quantity = reader["quantity"] == DBNull.Value ? 0 : (int)reader["quantity"];
                                tempProduct.total_price = reader["total_price"] == DBNull.Value ? 0 : (double)reader["total_price"];
                                tempProduct.status = reader["status"] == DBNull.Value ? "" : ((string)reader["status"]).ToString();
                                tempProduct.name = reader["name"] == DBNull.Value ? "" : ((string)reader["name"]).ToString();
                                tempProduct.brand_name = reader["brand_name"] == DBNull.Value ? "" : ((string)reader["brand_name"]).ToString();
                                tempProduct.created_at = reader["created_at"] == DBNull.Value ? "" : ((MySqlDateTime)reader["created_at"]).ToString();
                                tempProduct.updated_at = reader["updated_at"] == DBNull.Value ? "" : ((MySqlDateTime)reader["updated_at"]).ToString();
                                result.Add(tempProduct);

                            };
                        }
                        reader.Close();
                    }
                    conn.Close();
                }
                return StatusCode(200, result);
            }
            catch (Exception ex) { return BadRequest(ex.Message); }
        }

        // order-invoice-item
        [HttpPost]
        [Route("order-invoice-item")]
        public ActionResult OrderInvoiceItem(int? orderInvoiceItemId)
        {
            try
            {
                var result = new List<OrderInvoiceItem>();
                using (var conn = new MySqlConnection(_conString))
                {
                    conn.Open();
                    using (var cmd = new MySqlCommand())
                    {
                        string orderInvoiceItemIdValue = (orderInvoiceItemId.Equals(null) ? "" : orderInvoiceItemId.ToString());
                        cmd.CommandText = "SELECT * FROM order_invoice_items WHERE id = @orderInvoiceItemId";
                        cmd.Connection = conn;
                        cmd.Parameters.Add(new MySqlParameter { ParameterName = "@orderInvoiceItemId", DbType = DbType.String, Value = orderInvoiceItemId });
                        MySqlDataReader reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                var tempProduct = new OrderInvoiceItem();
                                tempProduct.id = reader["id"] == DBNull.Value ? 0 : (int)reader["id"];
                                tempProduct.order_invoice_id = reader["order_invoice_id"] == DBNull.Value ? 0 : (int)reader["order_invoice_id"];
                                tempProduct.product_id = reader["product_id"] == DBNull.Value ? 0 : (int)reader["product_id"];
                                tempProduct.quantity = reader["quantity"] == DBNull.Value ? 0 : (int)reader["quantity"];
                                tempProduct.total_price = reader["total_price"] == DBNull.Value ? 0 : (double)reader["total_price"];
                                tempProduct.status = reader["status"] == DBNull.Value ? "" : ((string)reader["status"]).ToString();
                                tempProduct.created_at = reader["created_at"] == DBNull.Value ? "" : ((DateTime)reader["created_at"]).ToString();
                                tempProduct.updated_at = reader["updated_at"] == DBNull.Value ? "" : ((DateTime)reader["updated_at"]).ToString();
                                result.Add(tempProduct);

                            };
                        }
                        reader.Close();
                    }
                    conn.Close();
                }
                return StatusCode(200, result);
            }
            catch (Exception ex) { return BadRequest(ex.Message); }
        }

        // post-order-invoice-item
        [HttpPost]
        [Route("post-order-invoice-item")]
        public ActionResult PostAnOrderInvoiceItem(int? loggedInUserId, [FromBody] OrderInvoiceItemBody body)
        {
            try
            {
                var result = new List<String>();
                bool orderInvoiceExists = false;
                int orderInvoiceId = 0;
                double payment_total = 0;
                using (var conn = new MySqlConnection(_conString))
                {

                    /* Pertama - tama, cek apakah pelanggan memiliki sebuah cart/invoice yang belum di-checkout ( dengan kata lain, nilai kolom statusnya = 'pending' )
                     * Apabila ada, ambil id invoice tersebut. */
                    conn.Open();
                    using (var cmd = new MySqlCommand())
                    {
                        string loggedInUserIdValue = (loggedInUserId.Equals(null) ? "" : loggedInUserId.ToString());
                        cmd.CommandText = "SELECT * FROM order_invoices WHERE customer_id = @loggedInUserId AND status = 'pending';";
                        cmd.Connection = conn;
                        cmd.Parameters.Add(new MySqlParameter { ParameterName = "@loggedInUserId", DbType = DbType.String, Value = loggedInUserIdValue });
                        MySqlDataReader reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            result.Add("Order invoice exists.");
                            orderInvoiceExists = true;
                            while (reader.Read())
                            {
                                orderInvoiceId = (int)reader["id"];
                            }
                        }
                        reader.Close();
                    }
                    conn.Close();

                    /* Apabila pelanggan belum punya order_invoice yang statusnya 'pending' ( Dengan kata lain, pelanggan tersebut merupakan pembeli baru atau sudah
                     * pernah checkout sebelumnya ), maka buat kolom order_invoice baru dengan nilai kolom customer_id sesuai yang dituju. */
                    if (!orderInvoiceExists)
                    {
                        conn.Open(); using (var cmd = new MySqlCommand())
                        {
                            cmd.Connection = conn;
                            cmd.Transaction = conn.BeginTransaction();
                            if (body.saveType == "add")
                            {
                                cmd.CommandText = "INSERT INTO order_invoices(customer_id,  customer_email,  customer_phone,  payment_method,  payment_total,  payment_verification_image,  status,  created_at,  updated_at) " +
                                                                     "VALUES (@customer_id, '' , '', '', 0, '', 'pending' , '', '');";
                            }
                            cmd.Parameters.AddRange(new MySqlParameter[]
                            {
                        new MySqlParameter { ParameterName="@customer_id", DbType = DbType.Int32, Value = loggedInUserId },
                            });
                            result.Add("OrderInvoice dari customer_id " + loggedInUserId.ToString() + " dan status 'pending' belum ada. Yang baru telah dibuat.");
                            try { int res = cmd.ExecuteNonQuery(); if (res == -1) { cmd.Transaction.Rollback(); } { cmd.Transaction.Commit(); } } catch { cmd.Transaction.Rollback(); throw; }
                        }
                        conn.Close();
                    }

                    /* Ulangi pengecekan seperti ditahap pertama -- sekarang seharusnya sudah ada order_invoice dengan customer_id sesuai yang dituju
                     * dan dengan status pending. Ubah nilai variabel boolean 'orderInvoiceExists' menjadi True. */
                    if (!orderInvoiceExists)
                    {
                        conn.Open();
                        using (var cmd = new MySqlCommand())
                        {
                            string loggedInUserIdValue = (loggedInUserId.Equals(null) ? "" : loggedInUserId.ToString());
                            cmd.CommandText = "SELECT * FROM order_invoices WHERE customer_id = @loggedInUserId AND status = 'pending';";
                            cmd.Connection = conn;
                            cmd.Parameters.Add(new MySqlParameter { ParameterName = "@loggedInUserId", DbType = DbType.String, Value = loggedInUserIdValue });
                            MySqlDataReader reader = cmd.ExecuteReader();
                            if (reader.HasRows)
                            {
                                result.Add("Order invoice exists.");
                                orderInvoiceExists = true;
                                while (reader.Read())
                                {
                                    orderInvoiceId = (int)reader["id"];
                                }
                            }
                            reader.Close();
                        }
                        conn.Close();
                    }

                    /* Ketika pelanggan sudah memiliki sebuah cart/invoice, maka ambil id invoice tersebut, kemudian masukkan data pada body request 
                     * ke table order_invoice_items, dengan nilai order_invoice_id = id invoice. */
                    if (orderInvoiceExists)
                    {
                        conn.Open(); using (var cmd = new MySqlCommand())
                        {
                            cmd.Connection = conn;
                            cmd.Transaction = conn.BeginTransaction();
                            if (body.saveType == "add")
                            {
                                cmd.CommandText = "INSERT INTO order_invoice_items(order_invoice_id,  product_id,  quantity,  total_price,  status,  created_at,  updated_at) " +
                                                                    "VALUES      (@order_invoice_id, @product_id, @quantity, @total_price, @status, @created_at, @updated_at);";
                            }
                            cmd.Parameters.AddRange(new MySqlParameter[]
                            {
                            new MySqlParameter { ParameterName="@order_invoice_id", DbType = DbType.Int32, Value = orderInvoiceId },
                            new MySqlParameter { ParameterName="@product_id", DbType = DbType.Int32, Value = body.product_id },
                            new MySqlParameter { ParameterName="@quantity", DbType = DbType.Int32, Value = body.quantity },
                            new MySqlParameter { ParameterName="@total_price", DbType = DbType.Double, Value = body.total_price },
                            new MySqlParameter { ParameterName="@status",DbType =  DbType.String, Value = String.IsNullOrEmpty(body.status) ? "" : body.status },
                            new MySqlParameter { ParameterName="@created_at",DbType =  DbType.String, Value = String.IsNullOrEmpty(body.created_at) ? "" : body.created_at },
                            new MySqlParameter { ParameterName="@updated_at", DbType = DbType.String, Value = String.IsNullOrEmpty(body.updated_at) ? "" : body.updated_at },
                            });
                            result.Add("POST request berhasil ditambahkan ke invoice dengan id " + orderInvoiceId.ToString());
                            try { int res = cmd.ExecuteNonQuery(); if (res == -1) { cmd.Transaction.Rollback(); } { cmd.Transaction.Commit(); } } catch { cmd.Transaction.Rollback(); throw; }
                        }
                        conn.Close();
                    }

                    /* Selanjutnya, kita perlu menghitung dan mengupdate nilai kolom payment_total di table order_invoices.
                     * Untuk itu, baca semua record pada tabel order_invoice_items yang memiliki order_invoice_id yang dicari sebelumnya, dan status = 'pending'.
                     * Untuk setiap record, tambahkan nilai kolom 'total_price' ke sebuah variabel bernama 'payment_total' yang sudah kita siapkan diawal fungsi ini. */
                    if (orderInvoiceExists)
                    {
                        conn.Open(); using (var cmd = new MySqlCommand())
                        {
                            string orderInvoiceIdValue = orderInvoiceId.ToString();
                            cmd.CommandText = "SELECT * FROM order_invoice_items WHERE order_invoice_id = @orderInvoiceId;";
                            cmd.Connection = conn;
                            cmd.Parameters.Add(new MySqlParameter { ParameterName = "@orderInvoiceId", DbType = DbType.String, Value = orderInvoiceIdValue });
                            MySqlDataReader reader = cmd.ExecuteReader();
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    payment_total += (double)reader["total_price"];
                                }
                                result.Add("Jumlah payment_total telah dihitung, yaitu " + payment_total.ToString());
                            }
                            reader.Close();
                        }
                        conn.Close();
                    }

                    /* Nilai variabel 'payment_total' yang sudah didapat kita berikan ke order_invoice yang dituju. */
                    if (orderInvoiceExists)
                    {
                        conn.Open(); using (var cmd = new MySqlCommand())
                        {
                            cmd.Connection = conn;
                            cmd.Transaction = conn.BeginTransaction();
                            if (body.saveType == "add")
                            {
                                cmd.CommandText = "UPDATE order_invoices SET payment_total = @paymentTotal WHERE id = @orderInvoiceId;";
                            }
                            cmd.Parameters.AddRange(new MySqlParameter[]
                            {
                            new MySqlParameter { ParameterName="@orderInvoiceId", DbType = DbType.Int32, Value = orderInvoiceId },
                            new MySqlParameter { ParameterName="@paymentTotal", DbType = DbType.Double, Value = payment_total },
                            });
                            result.Add("Nilai payment total terbaru sudah ditambahkan ke order_invoice dengan id " + orderInvoiceId.ToString());
                            result.Add(orderInvoiceId.ToString());
                            try { int res = cmd.ExecuteNonQuery(); if (res == -1) { cmd.Transaction.Rollback(); } { cmd.Transaction.Commit(); } } catch { cmd.Transaction.Rollback(); throw; }
                        }
                        conn.Close();
                    }

                }
                return StatusCode(200, result);
            }

            catch (Exception ex) { return BadRequest(ex.Message); }
        }

        // edit-order-invoice-item
        [HttpPost]
        [Route("edit-order-invoice-item")]
        public ActionResult EditOrderInvoiceItem([FromBody] OrderInvoiceItemBody body, int orderInvoiceItemId)
        {
            try
            {
                using (var conn = new MySqlConnection(_conString))
                {
                    conn.Open(); using (var cmd = new MySqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.Transaction = conn.BeginTransaction();
                        if (body.saveType == "edit")
                        {
                            cmd.CommandText = "UPDATE order_invoice_items SET order_invoice_id = @order_invoice_id,  product_id = @product_id,  quantity = @quantity,  total_price = @total_price,  created_at = @created_at,  updated_at = @updated_at " +
                                                          "WHERE id = @orderInvoiceItemId;";
                        }
                        cmd.Parameters.AddRange(new MySqlParameter[]
                        {
                        new MySqlParameter { ParameterName="@order_invoice_id", DbType = DbType.Int32, Value = body.order_invoice_id },
                        new MySqlParameter { ParameterName="@product_id", DbType = DbType.Int32, Value = body.product_id },
                        new MySqlParameter { ParameterName="@quantity", DbType = DbType.Int32, Value = body.quantity },
                        new MySqlParameter { ParameterName="@total_price", DbType = DbType.Double, Value = body.total_price },
                        new MySqlParameter { ParameterName="@status",DbType =  DbType.String, Value = String.IsNullOrEmpty(body.status) ? "" : body.status },
                        new MySqlParameter { ParameterName="@created_at",DbType =  DbType.String, Value = String.IsNullOrEmpty(body.created_at) ? "" : body.created_at },
                        new MySqlParameter { ParameterName="@updated_at", DbType = DbType.String, Value = String.IsNullOrEmpty(body.updated_at) ? "" : body.updated_at },
                        new MySqlParameter { ParameterName="@orderInvoiceId", DbType = DbType.Int32, Value = orderInvoiceItemId},
                        });

                        try { int res = cmd.ExecuteNonQuery(); if (res == -1) { cmd.Transaction.Rollback(); } { cmd.Transaction.Commit(); } } catch { cmd.Transaction.Rollback(); throw; }
                    }
                    conn.Close();
                }
                return Ok("Item invoice berhasil diedit");
            }
            catch (Exception ex) { return BadRequest(ex.Message); }
        }

        // delete-order-invoice-item
        [HttpPost]
        [Route("delete-order-invoice-item")]
        public ActionResult DeleteOrderInvoiceItem(int orderInvoiceItemId)
        {
            try
            {
                double remaining_payment_total = 0;
                int orderInvoiceId = 0;
                using (var conn = new MySqlConnection(_conString))
                {

                    conn.Open();
                    using (var cmd = new MySqlCommand())
                    {
                        string orderInvoiceItemIdValue = (orderInvoiceItemId.Equals(null) ? "" : orderInvoiceItemId.ToString());
                        cmd.CommandText = "SELECT * FROM order_invoice_items WHERE id = @orderInvoiceItemId";
                        cmd.Connection = conn;
                        cmd.Parameters.Add(new MySqlParameter { ParameterName = "@orderInvoiceItemId", DbType = DbType.String, Value = orderInvoiceItemIdValue });
                        MySqlDataReader reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                orderInvoiceId = (int)reader["order_invoice_id"];
                            };
                        }
                        reader.Close();
                    }
                    conn.Close();

                    conn.Open(); using (var cmd = new MySqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.Transaction = conn.BeginTransaction();
                        cmd.CommandText = "DELETE FROM order_invoice_items WHERE id = @orderInvoiceItemId;";
                        cmd.Parameters.AddRange(new MySqlParameter[]
                        {
                        new MySqlParameter { ParameterName="@orderInvoiceItemId", DbType = DbType.Int32, Value = orderInvoiceItemId},
                        });

                        try { int res = cmd.ExecuteNonQuery(); if (res == -1) { cmd.Transaction.Rollback(); } { cmd.Transaction.Commit(); } } catch { cmd.Transaction.Rollback(); throw; }
                    }
                    conn.Close();

                    /* Selanjutnya, kita perlu menghitung dan mengupdate nilai kolom payment_total di table order_invoices.
                     * Untuk itu, baca semua record pada tabel order_invoice_items yang memiliki order_invoice_id yang dicari sebelumnya, dan status = 'pending'.
                     * Untuk setiap record, tambahkan nilai kolom 'total_price' ke sebuah variabel bernama 'payment_total' yang sudah kita siapkan diawal fungsi ini. */
                    {
                        conn.Open(); using (var cmd = new MySqlCommand())
                        {
                            string orderInvoiceIdValue = orderInvoiceId.ToString();
                            cmd.CommandText = "SELECT * FROM order_invoice_items WHERE order_invoice_id = @orderInvoiceId  AND status = 'included' ;";
                            cmd.Connection = conn;
                            cmd.Parameters.Add(new MySqlParameter { ParameterName = "@orderInvoiceId", DbType = DbType.String, Value = orderInvoiceIdValue });
                            MySqlDataReader reader = cmd.ExecuteReader();
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    remaining_payment_total += (double)reader["total_price"];
                                }
                            }
                            reader.Close();
                        }
                        conn.Close();


                        /* Nilai variabel 'payment_total' yang sudah didapat kita berikan ke order_invoice yang dituju. */
                        {
                            conn.Open(); using (var cmd = new MySqlCommand())
                            {
                                cmd.Connection = conn;
                                cmd.Transaction = conn.BeginTransaction();
                                cmd.CommandText = "UPDATE order_invoices SET payment_total = @paymentTotal WHERE id = @orderInvoiceId;";
                                cmd.Parameters.AddRange(new MySqlParameter[]
                                {
                            new MySqlParameter { ParameterName="@orderInvoiceId", DbType = DbType.Int32, Value = orderInvoiceId },
                            new MySqlParameter { ParameterName="@paymentTotal", DbType = DbType.Double, Value = remaining_payment_total },
                                });
                                try { int res = cmd.ExecuteNonQuery(); if (res == -1) { cmd.Transaction.Rollback(); } { cmd.Transaction.Commit(); } } catch { cmd.Transaction.Rollback(); throw; }
                            }
                            conn.Close();



                        }
                        return Ok("Item invoice berhasil dihapus");
                    }
                }
            }
            catch (Exception ex) { return BadRequest(ex.Message); }
        }

        // order-item-inclusion
        [HttpPost]
        [Route("order-item-inclusion")]
        public ActionResult OrderItemInclusion(int orderInvoiceItemId, string orderInvoiceItemStatus)
        {
            try
            {
                string newStatus = "included";
                if (orderInvoiceItemStatus == "included")
                {
                    newStatus = "notIncluded";
                }

                double remaining_payment_total = 0;
                int orderInvoiceId = 0;
                using (var conn = new MySqlConnection(_conString))
                {

                    conn.Open();
                    using (var cmd = new MySqlCommand())
                    {
                        string orderInvoiceItemIdValue = (orderInvoiceItemId.Equals(null) ? "" : orderInvoiceItemId.ToString());
                        cmd.CommandText = "SELECT * FROM order_invoice_items WHERE id = @orderInvoiceItemId";
                        cmd.Connection = conn;
                        cmd.Parameters.Add(new MySqlParameter { ParameterName = "@orderInvoiceItemId", DbType = DbType.String, Value = orderInvoiceItemIdValue });
                        MySqlDataReader reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                orderInvoiceId = (int)reader["order_invoice_id"];
                            };
                        }
                        reader.Close();
                    }
                    conn.Close();

                    conn.Open(); using (var cmd = new MySqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.Transaction = conn.BeginTransaction();
                        cmd.CommandText = "UPDATE order_invoice_items SET status = @newStatus WHERE id = @orderInvoiceItemId;";
                        cmd.Parameters.AddRange(new MySqlParameter[]
                        {
                        new MySqlParameter { ParameterName="@orderInvoiceItemId", DbType = DbType.Int32, Value = orderInvoiceItemId},
                        new MySqlParameter { ParameterName="@newStatus", DbType = DbType.Int32, Value = newStatus},
                        });

                        try { int res = cmd.ExecuteNonQuery(); if (res == -1) { cmd.Transaction.Rollback(); } { cmd.Transaction.Commit(); } } catch { cmd.Transaction.Rollback(); throw; }
                    }
                    conn.Close();

                    /* Selanjutnya, kita perlu menghitung dan mengupdate nilai kolom payment_total di table order_invoices.
                     * Untuk itu, baca semua record pada tabel order_invoice_items yang memiliki order_invoice_id yang dicari sebelumnya, dan status = 'pending'.
                     * Untuk setiap record, tambahkan nilai kolom 'total_price' ke sebuah variabel bernama 'payment_total' yang sudah kita siapkan diawal fungsi ini. */
                    {
                        conn.Open(); using (var cmd = new MySqlCommand())
                        {
                            string orderInvoiceIdValue = orderInvoiceId.ToString();
                            cmd.CommandText = "SELECT * FROM order_invoice_items WHERE order_invoice_id = @orderInvoiceId AND status = 'included' ;";
                            cmd.Connection = conn;
                            cmd.Parameters.Add(new MySqlParameter { ParameterName = "@orderInvoiceId", DbType = DbType.String, Value = orderInvoiceIdValue });
                            MySqlDataReader reader = cmd.ExecuteReader();
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    remaining_payment_total += (double)reader["total_price"];
                                }
                            }
                            reader.Close();
                        }
                        conn.Close();


                        /* Nilai variabel 'payment_total' yang sudah didapat kita berikan ke order_invoice yang dituju. */
                        {
                            conn.Open(); using (var cmd = new MySqlCommand())
                            {
                                cmd.Connection = conn;
                                cmd.Transaction = conn.BeginTransaction();
                                cmd.CommandText = "UPDATE order_invoices SET payment_total = @paymentTotal WHERE id = @orderInvoiceId;";
                                cmd.Parameters.AddRange(new MySqlParameter[]
                                {
                            new MySqlParameter { ParameterName="@orderInvoiceId", DbType = DbType.Int32, Value = orderInvoiceId },
                            new MySqlParameter { ParameterName="@paymentTotal", DbType = DbType.Double, Value = remaining_payment_total },
                                });
                                try { int res = cmd.ExecuteNonQuery(); if (res == -1) { cmd.Transaction.Rollback(); } { cmd.Transaction.Commit(); } } catch { cmd.Transaction.Rollback(); throw; }
                            }
                            conn.Close();



                        }
                        return Ok("Item invoice berhasil diolah!");
                    }
                }
            }
            catch (Exception ex) { return BadRequest(ex.Message); }
        }


        // handle-quantity
        [HttpPost]
        [Route("handle-quantity")]
        public ActionResult OrderItemInclusion(int orderInvoiceItemId, int orderInvoiceId, int currentQuantity, double currentTotalPrice,  string act)
        {
            try
            {
                double remaining_payment_total = 0;
                using (var conn = new MySqlConnection(_conString))
                {
                    if (act == "plus") {
                        conn.Open(); using (var cmd = new MySqlCommand())
                        {
                            double newTotalPrice = (currentTotalPrice / currentQuantity) * (currentQuantity + 1);
                            cmd.Connection = conn;
                            cmd.Transaction = conn.BeginTransaction();
                            cmd.CommandText = "UPDATE order_invoice_items SET quantity = @newQuantity, total_price = @newTotalPrice WHERE id = @orderInvoiceItemId;";
                            cmd.Parameters.AddRange(new MySqlParameter[]
                            {
                                new MySqlParameter { ParameterName="@orderInvoiceItemId", DbType = DbType.Int32, Value = orderInvoiceItemId},
                                new MySqlParameter { ParameterName="@newQuantity", DbType = DbType.Int32, Value = currentQuantity + 1},
                                new MySqlParameter { ParameterName="@newTotalPrice", DbType = DbType.Double, Value = newTotalPrice},
                            });
                            System.Diagnostics.Debug.WriteLine("Operation is plus");
                            try { int res = cmd.ExecuteNonQuery(); if (res == -1) { cmd.Transaction.Rollback(); } { cmd.Transaction.Commit(); } } catch { cmd.Transaction.Rollback(); throw; }
                        }
                        conn.Close();
                    }
                    if (act == "minus")
                    {
                        conn.Open(); using (var cmd = new MySqlCommand())
                        {
                            double newTotalPrice = (currentTotalPrice / currentQuantity) * (currentQuantity - 1);
                            cmd.Connection = conn;
                            cmd.Transaction = conn.BeginTransaction();
                            cmd.CommandText = "UPDATE order_invoice_items SET quantity = @newQuantity, total_price = @newTotalPrice WHERE id = @orderInvoiceItemId;";
                            cmd.Parameters.AddRange(new MySqlParameter[]
                            {
                                new MySqlParameter { ParameterName="@orderInvoiceItemId", DbType = DbType.Int32, Value = orderInvoiceItemId},
                                new MySqlParameter { ParameterName="@newQuantity", DbType = DbType.Int32, Value = currentQuantity - 1},
                                new MySqlParameter { ParameterName="@newTotalPrice", DbType = DbType.Double, Value = newTotalPrice},
                            });
                            System.Diagnostics.Debug.WriteLine("Operation is minus");
                            try { int res = cmd.ExecuteNonQuery(); if (res == -1) { cmd.Transaction.Rollback(); } { cmd.Transaction.Commit(); } } catch { cmd.Transaction.Rollback(); throw; }
                        }
                        conn.Close();
                    }

                    /* Selanjutnya, kita perlu menghitung dan mengupdate nilai kolom payment_total di table order_invoices.
                     * Untuk itu, baca semua record pada tabel order_invoice_items yang memiliki order_invoice_id yang dicari sebelumnya, dan status = 'pending'.
                     * Untuk setiap record, tambahkan nilai kolom 'total_price' ke sebuah variabel bernama 'payment_total' yang sudah kita siapkan diawal fungsi ini. */
                    {
                        conn.Open(); using (var cmd = new MySqlCommand())
                        {
                            string orderInvoiceIdValue = orderInvoiceId.ToString();
                            cmd.CommandText = "SELECT * FROM order_invoice_items WHERE order_invoice_id = @orderInvoiceId AND status = 'included' ;";
                            cmd.Connection = conn;
                            cmd.Parameters.Add(new MySqlParameter { ParameterName = "@orderInvoiceId", DbType = DbType.String, Value = orderInvoiceIdValue });
                            MySqlDataReader reader = cmd.ExecuteReader();
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    remaining_payment_total += (double)reader["total_price"];
                                }
                            }
                            reader.Close();
                        }
                        conn.Close();


                        /* Nilai variabel 'payment_total' yang sudah didapat kita berikan ke order_invoice yang dituju. */
                        {
                            conn.Open(); using (var cmd = new MySqlCommand())
                            {
                                cmd.Connection = conn;
                                cmd.Transaction = conn.BeginTransaction();
                                cmd.CommandText = "UPDATE order_invoices SET payment_total = @paymentTotal WHERE id = @orderInvoiceId;";
                                cmd.Parameters.AddRange(new MySqlParameter[]
                                {
                            new MySqlParameter { ParameterName="@orderInvoiceId", DbType = DbType.Int32, Value = orderInvoiceId },
                            new MySqlParameter { ParameterName="@paymentTotal", DbType = DbType.Double, Value = remaining_payment_total },
                                });
                                try { int res = cmd.ExecuteNonQuery(); if (res == -1) { cmd.Transaction.Rollback(); } { cmd.Transaction.Commit(); } } catch { cmd.Transaction.Rollback(); throw; }
                            }
                            System.Diagnostics.Debug.WriteLine(orderInvoiceItemId.ToString() + "," + orderInvoiceId.ToString() + "," + currentQuantity.ToString() + "," + currentTotalPrice.ToString() + "," + act);
                            System.Diagnostics.Debug.WriteLine("Total price calculated");
                            conn.Close();



                        }
                        return Ok("Item invoice berhasil diolah!");
                    }
                }
            }
            catch (Exception ex) { return BadRequest(ex.Message); }
        }

        [HttpPost]
        [Route("delete-all-order-invoice-item")]
        public ActionResult DeleteAllOrderInvoiceItem(int orderInvoiceId)
        {
            try
            {
                using (var conn = new MySqlConnection(_conString))
                {

                    conn.Open(); using (var cmd = new MySqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.Transaction = conn.BeginTransaction();
                        cmd.CommandText = "DELETE FROM order_invoice_items WHERE order_invoice_id = @orderInvoiceId;";
                        cmd.Parameters.AddRange(new MySqlParameter[]
                        {
                        new MySqlParameter { ParameterName="@orderInvoiceId", DbType = DbType.Int32, Value = orderInvoiceId},
                        });

                        try { int res = cmd.ExecuteNonQuery(); if (res == -1) { cmd.Transaction.Rollback(); } { cmd.Transaction.Commit(); } } catch { cmd.Transaction.Rollback(); throw; }
                    }
                    conn.Close();


                    conn.Open(); using (var cmd = new MySqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.Transaction = conn.BeginTransaction();
                        cmd.CommandText = "UPDATE order_invoices SET payment_total = @paymentTotal WHERE id = @orderInvoiceId;";
                        cmd.Parameters.AddRange(new MySqlParameter[]
                        {
                    new MySqlParameter { ParameterName="@orderInvoiceId", DbType = DbType.Int32, Value = orderInvoiceId },
                    new MySqlParameter { ParameterName="@paymentTotal", DbType = DbType.Double, Value = 0 },
                        });
                        try { int res = cmd.ExecuteNonQuery(); if (res == -1) { cmd.Transaction.Rollback(); } { cmd.Transaction.Commit(); } } catch { cmd.Transaction.Rollback(); throw; }
                    }
                    conn.Close();



                        }
                        return Ok("Item invoice berhasil dihapus");
                    }
            catch (Exception ex) { return BadRequest(ex.Message); }
        }



    }
}
