﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MySqlConnector;
using System.Data;
using WebApi.Models;
using WebApi.Method;

// AUTHHH
using Microsoft.AspNetCore.Authorization;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private IConfiguration _configuration;
        private string _conString = "";

        public ProductController(IConfiguration configuration)
        {
            _configuration = configuration;
            _conString = configuration["ConnectionStrings:Default"];
        }

        // list-all-products
        [HttpGet]
        [Route("list-all-products")]
        public ActionResult ListAllProducts(string? search, string? sortBy, string? brandIds, double? minPrice, double? maxPrice)
        {
            HttpContext.Response.Headers.Add("x-my-custom-header", "individual response");
            try
            {
                var result = new List<Product1>();
                
                using (var conn = new MySqlConnection(_conString))
                {
                    conn.Open();
                    using (var cmd = new MySqlCommand())
                    {
                        int[] brandIdsArray = new int[] { };
                        int lastBrandIdValue = 0;
                        int lastBrandIdIndex = 0;
                        string searchh = String.IsNullOrEmpty(search) ? "" : search;
                        double? minPriceValue = String.IsNullOrEmpty(minPrice.ToString()) ? 0 : minPrice;
                        double? maxPriceValue = String.IsNullOrEmpty(maxPrice.ToString()) ? 999999999 : maxPrice;
                        cmd.CommandText = "SELECT * FROM products INNER JOIN brands ON products.brand_id = brands.id WHERE products.name LIKE @search AND (price BETWEEN @minPrice AND @maxPrice) ";
 
                            if (!String.IsNullOrEmpty(brandIds))
                        {
                            cmd.CommandText += " AND ( ";
                            brandIdsArray = brandIds.Split(',').Select(n => Convert.ToInt32(n)).ToArray();
                            lastBrandIdValue = brandIdsArray.Last();
                            lastBrandIdIndex = Array.IndexOf(brandIdsArray, lastBrandIdValue);
                        }

                        foreach (int brandIdValue in brandIdsArray)
                        {
                            if (Array.IndexOf(brandIdsArray, brandIdValue) != lastBrandIdIndex)
                            {
                                string paramName = "@brand_id_" + brandIdValue;
                                cmd.Parameters.Add(new MySqlParameter { ParameterName = paramName, DbType = DbType.Int32, Value = brandIdValue });
                                cmd.CommandText += " brand_id = " + paramName + " OR ";
                            } else
                            {
                                string paramName = "@brand_id_" + brandIdValue;
                                cmd.Parameters.Add(new MySqlParameter { ParameterName = paramName, DbType = DbType.Int32, Value = brandIdValue });
                                cmd.CommandText += " brand_id = " + paramName + " )";
                            }
                        }

                        string sortByValue = String.IsNullOrEmpty(sortBy) ? "" : sortBy;
                        if (sortBy == "Terbaru")
                        {
                            cmd.CommandText += " ORDER BY products.id DESC ;" ;
                        } else if (sortBy == "Terlaris")
                        {
                            cmd.CommandText += " ORDER BY sold DESC ;";
                        }
                            else if (sortBy == "Harga Tertinggi")
                        {
                            cmd.CommandText += " ORDER BY price DESC ;";
                        }
                            else if (sortBy == "Harga Terendah")
                        {
                            cmd.CommandText += " ORDER BY price ASC ;";
                        }

                        System.Diagnostics.Debug.WriteLine(cmd.CommandText);


                        cmd.Connection = conn;
                        cmd.Parameters.Add(new MySqlParameter { ParameterName = "@search", DbType = DbType.String, Value = "%" + searchh + "%" });
                        cmd.Parameters.Add(new MySqlParameter { ParameterName = "@minPrice", DbType = DbType.Double, Value = minPriceValue });
                        cmd.Parameters.Add(new MySqlParameter { ParameterName = "@maxPrice", DbType = DbType.Double, Value = maxPriceValue });
                        MySqlDataReader reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                var tempProduct = new Product1();
                                tempProduct.image = reader["image"] == DBNull.Value ? "" : (string)reader["image"];
                                tempProduct.id = reader["id"] == DBNull.Value ? 0 : (int)reader["id"];
                                tempProduct.name = reader["name"] == DBNull.Value ? "" : (string)reader["name"];
                                tempProduct.description = reader["description"] == DBNull.Value ? "" : (string)reader["description"];
                                tempProduct.brand_id = reader["brand_id"] == DBNull.Value ? 0 : (int)reader["brand_id"];
                                tempProduct.price = reader["price"] == DBNull.Value ? 0 : (double)reader["price"];
                                tempProduct.sold = reader["sold"] == DBNull.Value ? 0 : (int)reader["sold"];
                                tempProduct.rating = reader["rating"] == DBNull.Value ? 0 : (int)reader["rating"];
                                tempProduct.brand_name = reader["brand_name"] == DBNull.Value ? "" : (string)reader["brand_name"];
                                result.Add(tempProduct);

                            };}reader.Close();}conn.Close();}return StatusCode(200, result);}catch (Exception ex){return BadRequest(ex.Message);}
        }
        // list-products-by-category
        [HttpGet]
        [Route("list-products-by-category")]
        public ActionResult ListProductsByCategory(string? brandId, string? sortBy, double? minPrice, double? maxPrice)
        {
            HttpContext.Response.Headers.Add("x-my-custom-header", "individual response");
            try
            {
                var result = new List<Product1>();
                using (var conn = new MySqlConnection(_conString))
                {
                    conn.Open();
                    using (var cmd = new MySqlCommand())
                    {
                        string brandIdValue = String.IsNullOrEmpty(brandId) ? "" : brandId;
                        double? minPriceValue = String.IsNullOrEmpty(minPrice.ToString()) ? 0 : minPrice;
                        double? maxPriceValue = String.IsNullOrEmpty(maxPrice.ToString()) ? 999999999 : maxPrice;
                        cmd.CommandText = "SELECT * FROM products INNER JOIN brands ON products.brand_id = brands.id WHERE products.brand_id LIKE @brandId AND (price BETWEEN @minPrice AND @maxPrice) ";

                        string sortByValue = String.IsNullOrEmpty(sortBy) ? "" : sortBy;
                        if (sortBy == "Terbaru")
                        {
                            cmd.CommandText += " ORDER BY products.id DESC ;";
                        }
                        else if (sortBy == "Terlaris")
                        {
                            cmd.CommandText += " ORDER BY sold DESC ;";
                        }
                        else if (sortBy == "Harga Tertinggi")
                        {
                            cmd.CommandText += " ORDER BY price DESC ;";
                        }
                        else if (sortBy == "Harga Terendah")
                        {
                            cmd.CommandText += " ORDER BY price ASC ;";
                        }

                        cmd.Connection = conn;
                        cmd.Parameters.Add(new MySqlParameter { ParameterName = "@brandId", DbType = DbType.String, Value = "%" + brandIdValue + "%" });
                        cmd.Parameters.Add(new MySqlParameter { ParameterName = "@minPrice", DbType = DbType.Double, Value = minPriceValue });
                        cmd.Parameters.Add(new MySqlParameter { ParameterName = "@maxPrice", DbType = DbType.Double, Value = maxPriceValue });
                        MySqlDataReader reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                var tempProduct = new Product1();
                                tempProduct.image = reader["image"] == DBNull.Value ? "" : (string)reader["image"];
                                tempProduct.id = reader["id"] == DBNull.Value ? 0 : (int)reader["id"];
                                tempProduct.name = reader["name"] == DBNull.Value ? "" : (string)reader["name"];
                                tempProduct.description = reader["description"] == DBNull.Value ? "" : (string)reader["description"];
                                tempProduct.brand_id = reader["brand_id"] == DBNull.Value ? 0 : (int)reader["brand_id"];
                                tempProduct.price = reader["price"] == DBNull.Value ? 0 : (double)reader["price"];
                                tempProduct.sold = reader["sold"] == DBNull.Value ? 0 : (int)reader["sold"];
                                tempProduct.rating = reader["rating"] == DBNull.Value ? 0 : (int)reader["rating"];
                                tempProduct.brand_name = reader["brand_name"] == DBNull.Value ? "" : (string)reader["brand_name"];
                                result.Add(tempProduct);

                            };
                        }
                        reader.Close();
                    }
                    conn.Close();
                }
                return StatusCode(200, result);
            }
            catch (Exception ex) { return BadRequest(ex.Message); }
        }

        // list-products-invoice_id
        [HttpGet]
        [Route("list-products-by-invoice")]
        public ActionResult ListProductsByInvoice(string? invoiceId)
        {
            HttpContext.Response.Headers.Add("x-my-custom-header", "individual response");
            try
            {
                var result = new List<OrderInvoiceItem>();
                using (var conn = new MySqlConnection(_conString))
                {
                    conn.Open();
                    using (var cmd = new MySqlCommand())
                    {
                        string invoiceIdValue = String.IsNullOrEmpty(invoiceId) ? "" : invoiceId;
                        cmd.CommandText = "SELECT products.image, order_invoice_items.id, order_invoice_items.order_invoice_id, products.name, order_invoice_items.status AS invoice_item_status, order_invoice_items.quantity, order_invoice_items.total_price FROM products " + 
                            "JOIN order_invoice_items ON products.id = order_invoice_items.product_id " + 
                            "JOIN order_invoices ON order_invoice_items.order_invoice_id = order_invoices.id " +
                            "WHERE order_invoices.id = @invoiceId;";
                        cmd.Connection = conn;
                        cmd.Parameters.Add(new MySqlParameter { ParameterName = "@invoiceId", DbType = DbType.String, Value = invoiceIdValue  });
                        MySqlDataReader reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                var tempProduct = new OrderInvoiceItem();
                                tempProduct.image = reader["image"] == DBNull.Value ? "" : (string)reader["image"];
                                tempProduct.id = reader["id"] == DBNull.Value ? 0 : (int)reader["id"];
                                tempProduct.order_invoice_id = reader["order_invoice_id"] == DBNull.Value ? 0 : (int)reader["order_invoice_id"];
                                tempProduct.name = reader["name"] == DBNull.Value ? "" : (string)reader["name"];
                                tempProduct.quantity = reader["quantity"] == DBNull.Value ? 0 : (int)reader["quantity"];
                                tempProduct.total_price = reader["total_price"] == DBNull.Value ? 0 : (double)reader["total_price"];
                                tempProduct.status = reader["invoice_item_status"] == DBNull.Value ? "" : (string)reader["invoice_item_status"];
                                result.Add(tempProduct);

                            };
                        }
                        reader.Close();
                    }
                    conn.Close();
                }
                return StatusCode(200, result);
            }
            catch (Exception ex) { return BadRequest(ex.Message); }
        }

        // product-detail
        [HttpGet]
        [Route("product-detail")]
        public ActionResult ProductDetail(int? productId)
        {
            try
            {
                var result = new List<Product1>();
                using (var conn = new MySqlConnection(_conString))
                {
                    conn.Open();
                    using (var cmd = new MySqlCommand())
                    {
                        string productIdValue = (productId.Equals(null) ? "" : productId.ToString());
                        cmd.CommandText = "SELECT * FROM products INNER JOIN brands ON products.brand_id = brands.id WHERE products.id = @productId";
                        cmd.Connection = conn;
                        cmd.Parameters.Add(new MySqlParameter { ParameterName = "@productId", DbType = DbType.String, Value = productIdValue });
                        MySqlDataReader reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                var tempProduct = new Product1();
                                tempProduct.id = reader["id"] == DBNull.Value ? 0 : (int)reader["id"];
                                tempProduct.name = reader["name"] == DBNull.Value ? "" : (string)reader["name"];
                                tempProduct.description = reader["description"] == DBNull.Value ? "" : (string)reader["description"];
                                tempProduct.brand_id = reader["brand_id"] == DBNull.Value ? 0 : (int)reader["brand_id"];
                                tempProduct.price = reader["price"] == DBNull.Value ? 0 : (double)reader["price"];
                                tempProduct.sold = reader["sold"] == DBNull.Value ? 0 : (int)reader["sold"];
                                tempProduct.rating = reader["rating"] == DBNull.Value ? 0 : (int)reader["rating"];
                                tempProduct.image = reader["image"] == DBNull.Value ? "" : (string)reader["image"];
                                tempProduct.brand_name = reader["brand_name"] == DBNull.Value ? "" : (string)reader["brand_name"];
                                result.Add(tempProduct);

                            };
                        }
                        reader.Close();
                    }
                    conn.Close();
                }
                return StatusCode(200, result);
            }
            catch (Exception ex) { return BadRequest(ex.Message); }
        }

        // post-product
        [HttpPost]
        [Authorize(Roles = "admin")]
        [Route("post-product")]
        public ActionResult PostAProduct([FromBody] Product1Body body)
        {
            try
            {
                using (var conn = new MySqlConnection(_conString))
                {
                    conn.Open(); using (var cmd = new MySqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.Transaction = conn.BeginTransaction();
                        if (body.saveType == "add")
                        {
                            cmd.CommandText = "INSERT INTO products(name,  description,  brand_id,  price,  sold,  rating,  image) " +
                                                          "VALUES (@name, @description, @brand_id, @price, @sold, @rating, @image);";
                        }
                        cmd.Parameters.AddRange(new MySqlParameter[]
                        {
                        new MySqlParameter { ParameterName="@name", DbType = DbType.String, Value = String.IsNullOrEmpty(body.name) ? "" : body.name },
                        new MySqlParameter { ParameterName="@description", DbType = DbType.String, Value = String.IsNullOrEmpty(body.description) ? "" : body.description },
                        new MySqlParameter { ParameterName="@brand_id", DbType = DbType.Int32, Value = body.brand_id},
                        new MySqlParameter { ParameterName="@price", DbType = DbType.Double, Value = body.price },
                        new MySqlParameter { ParameterName="@sold", DbType = DbType.Int32, Value = body.sold},
                        new MySqlParameter { ParameterName="@rating", DbType = DbType.Int32, Value = body.rating},
                        new MySqlParameter { ParameterName="@image", DbType = DbType.String, Value = String.IsNullOrEmpty(body.image) ? "" : body.image },
                        });

                        try { int res = cmd.ExecuteNonQuery(); if (res == -1) { cmd.Transaction.Rollback(); }  { cmd.Transaction.Commit(); } } catch { cmd.Transaction.Rollback(); throw; }
                    } conn.Close();
                } return Ok("Produk baru berhasil ditambahkan");}catch (Exception ex){return BadRequest(ex.Message);}
        }

        // edit-product
        [HttpPost]
        [Authorize(Roles = "admin")]
        [Route("edit-product")]
        public ActionResult EditProduct([FromBody] Product1Body body, int productId)
        {
            try
            {
                using (var conn = new MySqlConnection(_conString))
                {
                    conn.Open(); using (var cmd = new MySqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.Transaction = conn.BeginTransaction();
                        if (body.saveType == "edit")
                        {
                            cmd.CommandText = "UPDATE products SET name = @name,  description = @description,  brand_id = @brand_id,  price = @price,  sold = @sold,  rating = @rating,  image = @image " +
                                                          "WHERE id = @productId;";
                        }
                        cmd.Parameters.AddRange(new MySqlParameter[]
                        {
                        new MySqlParameter { ParameterName="@name", DbType = DbType.String, Value = String.IsNullOrEmpty(body.name) ? "" : body.name },
                        new MySqlParameter { ParameterName="@description", DbType = DbType.String, Value = String.IsNullOrEmpty(body.description) ? "" : body.description },
                        new MySqlParameter { ParameterName="@brand_id", DbType = DbType.Int32, Value = body.brand_id},
                        new MySqlParameter { ParameterName="@price", DbType = DbType.Double, Value = body.price },
                        new MySqlParameter { ParameterName="@sold", DbType = DbType.Int32, Value = body.sold},
                        new MySqlParameter { ParameterName="@rating", DbType = DbType.Int32, Value = body.rating},
                        new MySqlParameter { ParameterName="@image", DbType = DbType.String, Value = String.IsNullOrEmpty(body.image) ? "" : body.image },
                        new MySqlParameter { ParameterName="@productId", DbType = DbType.Int32, Value = productId},
                        });

                        try { int res = cmd.ExecuteNonQuery(); if (res == -1) { cmd.Transaction.Rollback(); } { cmd.Transaction.Commit(); } } catch { cmd.Transaction.Rollback(); throw; }
                    }
                    conn.Close();
                }
                return Ok("Produk berhasil diedit");
            }
            catch (Exception ex) { return BadRequest(ex.Message); }
        }

        // delete-product
        [HttpPost]
        [Authorize(Roles = "admin")]
        [Route("delete-product")]
        public ActionResult DeleteProduct(int productId)
        {
            try
            {
                using (var conn = new MySqlConnection(_conString))
                {
                    conn.Open(); using (var cmd = new MySqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.Transaction = conn.BeginTransaction();
                        cmd.CommandText = "DELETE FROM products WHERE id = @productId;";
                        cmd.Parameters.AddRange(new MySqlParameter[]
                        {
                        new MySqlParameter { ParameterName="@productId", DbType = DbType.Int32, Value = productId},
                        });

                        try { int res = cmd.ExecuteNonQuery(); if (res == -1) { cmd.Transaction.Rollback(); } { cmd.Transaction.Commit(); } } catch { cmd.Transaction.Rollback(); throw; }
                    }
                    conn.Close();
                }
                return Ok("Produk berhasil dihapus");
            }
            catch (Exception ex) { return BadRequest(ex.Message); }
        }

    }
}
