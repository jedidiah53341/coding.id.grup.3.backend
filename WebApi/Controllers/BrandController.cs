﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MySqlConnector;
using System.Data;
using WebApi.Models;
using WebApi.Method;

// AUTHHH
using Microsoft.AspNetCore.Authorization;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BrandController : ControllerBase
    {
        private IConfiguration _configuration;
        private string _conString = "";

        public BrandController(IConfiguration configuration)
        {
            _configuration = configuration;
            _conString = configuration["ConnectionStrings:Default"];
        }

        // list-all-brands
        [HttpGet]
        [Route("list-all-brands")]
        public ActionResult ListAllbrands(string? search, string? sortBy, string? category, int? minPrice, int? maxPrice)
        {
            try
            {
                var result = new List<Brand>();
                using (var conn = new MySqlConnection(_conString))
                {
                    conn.Open();
                    using (var cmd = new MySqlCommand())
                    {
                        string searchh = String.IsNullOrEmpty(search) ? "" : search;
                        cmd.CommandText = "SELECT * FROM brands WHERE brand_name LIKE @search";
                        cmd.Connection = conn;
                        cmd.Parameters.Add(new MySqlParameter { ParameterName = "@search", DbType = DbType.String, Value = "%" + searchh + "%" });
                        MySqlDataReader reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                var tempProduct = new Brand();
                                tempProduct.id = reader["id"] == DBNull.Value ? 0 : (int)reader["id"];
                                tempProduct.brand_name = reader["brand_name"] == DBNull.Value ? "" : (string)reader["brand_name"];
                                tempProduct.description = reader["description"] == DBNull.Value ? "" : (string)reader["description"];
                                tempProduct.image = reader["image"] == DBNull.Value ? "" : (string)reader["image"];
                                result.Add(tempProduct);
                            };}reader.Close();}conn.Close();}return StatusCode(200, result);}catch (Exception ex){return BadRequest(ex.Message);}
        }

        // list-all-brands-names-and-id
        [HttpGet]
        [Route("list-all-brands-name-and-id")]
        public ActionResult ListAllBrandsNameAndId(string? search, string? sortBy, string? category, int? minPrice, int? maxPrice)
        {
            try
            {
                var result = new List<Brand>();
                using (var conn = new MySqlConnection(_conString))
                {
                    conn.Open();
                    using (var cmd = new MySqlCommand())
                    {
                        string searchh = String.IsNullOrEmpty(search) ? "" : search;
                        cmd.CommandText = "SELECT * FROM brands WHERE brand_name LIKE @search";
                        cmd.Connection = conn;
                        cmd.Parameters.Add(new MySqlParameter { ParameterName = "@search", DbType = DbType.String, Value = "%" + searchh + "%" });
                        MySqlDataReader reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                var tempProduct = new Brand();
                                tempProduct.id = reader["id"] == DBNull.Value ? 0 : (int)reader["id"];
                                tempProduct.brand_name = reader["brand_name"] == DBNull.Value ? "" : (string)reader["brand_name"];
                                result.Add(tempProduct);
                            };
                        }
                        reader.Close();
                    }
                    conn.Close();
                }
                return StatusCode(200, result);
            }
            catch (Exception ex) { return BadRequest(ex.Message); }
        }

        // post-brand
        [HttpPost]
        [Authorize(Roles = "admin")]
        [Route("post-brand")]
        public ActionResult PostABrand([FromBody] BrandBody body)
        {
            try
            {
                using (var conn = new MySqlConnection(_conString))
                {
                    conn.Open(); using (var cmd = new MySqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.Transaction = conn.BeginTransaction();
                        if (body.saveType == "add")
                        {
                            cmd.CommandText = "INSERT INTO brands   (brand_name,  description,  image) " +
                                                          "VALUES (@brand_name, @description, @image);";
                        }
                        cmd.Parameters.AddRange(new MySqlParameter[]
                        {
                        new MySqlParameter { ParameterName="@brand_name", DbType = DbType.String, Value = String.IsNullOrEmpty(body.brand_name) ? "" : body.brand_name },
                        new MySqlParameter { ParameterName="@description", DbType = DbType.String, Value = String.IsNullOrEmpty(body.description) ? "" : body.description },
                        new MySqlParameter { ParameterName="@image", DbType = DbType.String, Value = String.IsNullOrEmpty(body.image) ? "" : body.image },
                        });

                        try { int res = cmd.ExecuteNonQuery(); if (res == -1) { cmd.Transaction.Rollback(); } { cmd.Transaction.Commit(); } } catch { cmd.Transaction.Rollback(); throw; }
                    } conn.Close();
                } return Ok("success");}catch (Exception ex){return BadRequest(ex.Message);}
        }

        // edit-brand
        [HttpPost]
        [Authorize(Roles = "admin")]
        [Route("edit-brand")]
        public ActionResult EditBrand([FromBody] BrandBody body, int brandId)
        {
            try
            {
                using (var conn = new MySqlConnection(_conString))
                {
                    conn.Open(); using (var cmd = new MySqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.Transaction = conn.BeginTransaction();
                        if (body.saveType == "edit")
                        {
                            cmd.CommandText = "UPDATE brands SET brand_name = @brand_name,  description = @description, image = @image " +
                                                          "WHERE id = @brandId;";
                        }
                        cmd.Parameters.AddRange(new MySqlParameter[]
                        {
                        new MySqlParameter { ParameterName="@brand_name", DbType = DbType.String, Value = String.IsNullOrEmpty(body.brand_name) ? "" : body.brand_name },
                        new MySqlParameter { ParameterName="@description", DbType = DbType.String, Value = String.IsNullOrEmpty(body.description) ? "" : body.description },
                        new MySqlParameter { ParameterName="@image", DbType = DbType.String, Value = String.IsNullOrEmpty(body.image) ? "" : body.image },
                        new MySqlParameter { ParameterName="@brandId", DbType = DbType.Int32, Value = brandId},
                        });

                        try { int res = cmd.ExecuteNonQuery(); if (res == -1) { cmd.Transaction.Rollback(); } { cmd.Transaction.Commit(); } } catch { cmd.Transaction.Rollback(); throw; }
                    }
                    conn.Close();
                }
                return Ok("Merk berhasil diedit");
            }
            catch (Exception ex) { return BadRequest(ex.Message); }
        }

        // delete-brand
        [HttpPost]
        [Authorize(Roles = "admin")]
        [Route("delete-brand")]
        public ActionResult DeleteBrand(int brandId)
        {
            try
            {
                using (var conn = new MySqlConnection(_conString))
                {
                    conn.Open(); using (var cmd = new MySqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.Transaction = conn.BeginTransaction();
                        cmd.CommandText = "DELETE FROM brands WHERE id = @brandId;";
                        cmd.Parameters.AddRange(new MySqlParameter[]
                        {
                        new MySqlParameter { ParameterName="@brandId", DbType = DbType.Int32, Value = brandId},
                        });

                        try { int res = cmd.ExecuteNonQuery(); if (res == -1) { cmd.Transaction.Rollback(); } { cmd.Transaction.Commit(); } } catch { cmd.Transaction.Rollback(); throw; }
                    }
                    conn.Close();
                }
                return Ok("Merk berhasil dihapus");
            }
            catch (Exception ex) { return BadRequest(ex.Message); }
        }

    }
}
