using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using WebApi.Method;

var builder = WebApplication.CreateBuilder(args);
// Add services to the container.
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

// CONFIGURATION FROM appsettings.json
// SOURCE: https://stackoverflow.com/a/69722959
ConfigurationManager configuration = builder.Configuration;

// fill our method with configuration
Token.Init(configuration);
Email.Init(configuration);


// CORS
builder.Services.AddCors(options =>
{options.AddPolicy("AllowAll",builder =>{builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader();});});

// AUTHHHH
string jwtKey = "terserah mau bikin key apa yang penting string"; //builder.Configuration["jwt:key"];
builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer((options) =>
    {
        options.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuerSigningKey = true,
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtKey)),
            ValidateIssuer = false,
            ValidateAudience = false,


        };
    });

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment()) { app.UseSwagger(); app.UseSwaggerUI();}

app.UseCors("AllowAll"); // CORS
app.UseHttpsRedirection();
app.UseAuthentication(); // AUTHHHH
app.UseAuthorization();
app.MapControllers();
app.Run();
